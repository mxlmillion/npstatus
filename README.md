Switch branches to 'redzone-example' to see this tool in action

### USAGE
```
	status.pl <flags>
	status.pl -h/--help
```
### FLAGS
```
	-D/--debug
		Prints out debug info. Use multiple times to increase verbosity.

	-e/--environment <string>
		Specifies the target enivronment. Valid values are: 'np' & 'pl'. 'np' is default.

	--format/--output-format <string>
		Specifies how results are printed. Valid values are: 'utf8' & 'json'. 'utf8' is default.

		JSON support is experimental

	--group/-g
		Changes user's group. Running bulk commands like --all, --unsafe, etc. run only apps that are part of your group.

		Available groups are: spo, cpo, lat, hawkeye

		'spo' is the default group.

	-h/--help
		Prints this message. This message is also printed when no arguments are supplied.

	-l/--line <string>
		The line you want to test against. For example: p1, p2, e1

	-L/--list
		Print out a list of avaible services for a line.

	--poll
		Continuously run the test. Use ctrl-c to abort.

	-P/--parallel
		Specifies whether status.pl should utilize fork() to speed up testing of a service that have large number of instances.

	-r/--range <start int>-[<end int>]

		To scan only host 1: -r 1-1
		To scan hosts 23-43: -r 23-43
		To scan all hosts except the first 8: -r 9-

	--raw
		Do not parse returned content, but instead return raw output from application's status endpoint.

	-s/--service-name <string>
		The application (or service) to test. To get a list of available services, use the -l flag.

	-t/--test-type <string>
		Test type. By default, this is the "scrape" functionality, where each instance's /internal/status/html endpoint is queried
		and evaluated. There are 3 test types:

		scrape - Evaluate each instance's /internal/status/html endpoint.
		extvip (not ready) - Like scrape, but evaluates the external VIP address.
		intvip (not ready) - Like scrape, but evaluates a VIP that is only accessible within our network.

	--test-all/--all
		Tests all available services the script knows about.


	--test-all-safe/--safe
		Tests only apps that are Safe.


	--test-all-unsafe/--unsafe
		Tests only apps that are Unsafe.

	--tier <string>
		Specify which tier of the service to test.

		Available tiers: tomcat, apache

		Alternatively, a numerical value can be passed as well. For example, to test tier 1 of the Oauth application, which is Apache, pass either numeric '1' or the string 'apache'.

```
### ENVIRONMENT VARIABLES
```
	 $ADPW  $NPSTAT_ADPW		Used for SSH logins and Basic auth for VIP checks.
	 $ADUSER  $NPSTAT_ADUSER	Used for SSH logins and Basic auth for VIP checks.
	 $NPSTAT_GROUP				Configures your group membership. Running bulk commands like --all, --unsafe, etc. run only apps that are part of your group. 'spo' is the default group.
```