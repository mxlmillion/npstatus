package NPStatus::Report;

use strict;
use warnings;
use Sys::Hostname;


our $output_format = '';
our $totalsuccess = 0;
our $totalfailure = 0;
our $badones      = 0;
our @CONNECTIONS;
our @UPDATES;
our @UNKNOWN;
our %CONNECTIONS;
our %UPDATES;
our %UNKNOWN;
our %SUCCESS;
our %MASTER_STRUCT;
our %HostHash;
our %P_ResultArrayByHost;
our $service_name;
our $succeeded;
our $failed;
our $type;
our $service;
our $status;
our $success = 0;
our $failure = 0;
our $PATH  = ".:$ENV{'PATH'}";
our $count    = 0;
our $badcount = 0;
our $LOGFILE;
our @BADHOST;
our $succeed;
our $v1 = 0;
our $Begin = Timestamp();
our $End = '';
#our ($TimeNow) = Timestamp();
our $year = 0;
our $mon = 0;
our $day = 0;
our $hr = 0;
our $min = 0;
our $sec = 0;


sub Timestamp {
    ( $sec, $min, $hr, $day, $mon, $year, $v1 ) = localtime(time);
    FixTime();
    #my $Date  = "$year$mon$day-$hr$min$sec";
    my $TimeNow = "$hr:$min:$sec";

    return ($TimeNow);
}

sub FixTime {
    $year += 1900;
    $mon  += 1;
    $v1  = length($mon);
    $mon = "0$mon" if ( $v1 == 1 );
    $v1  = length($day);
    $day = "0$day" if ( $v1 == 1 );
    $v1  = length($hr);
    $hr  = "0$hr" if ( $v1 == 1 );
    $v1  = length($min);
    $min = "0$min" if ( $v1 == 1 );
    $v1  = length($sec);
    $sec = "0$sec" if ( $v1 == 1 );
}

sub printStatistics {

    my ( $output_format ) = $NPStatus::Init::output_format;

    $End = Timestamp();

    if ( $output_format eq 'utf8' ) {

        if( $NPStatus::Init::parallelize ) {

            # A few things we want to see in our DEFAULT status report:
            #
            #   1. each host's successful and failed tests
            #   2. Any errors encountered while testing that host
            #   3. After all hosts are printed with their relavent stats, a summary showing total number of successes & failures.

            use Data::Dumper;

            my $total_scount = 0;
            my $total_fcount = 0;
            my $total_ecount = 0;
            my %stotal;
            my %ftotal;

            # Print each instance and stats for it, while collecting cumulative data for the summary.
            foreach my $host (sort(keys %MASTER_STRUCT )){
                #print Data::Dumper->Dump([$MASTER_STRUCT{$host}]);
                foreach my $port (keys %{ $MASTER_STRUCT{$host} }){

                    my $scount = 0;
                    my $fcount = 0;
                    my $ecount = 0;
                    if( defined($MASTER_STRUCT{$host}{$port}{"success"})) {
                        foreach (keys %{ $MASTER_STRUCT{$host}{$port}{"success"} }) {
                            $stotal{$_} = 1 unless exists $stotal{$_};
                            $scount++;
                            $total_scount++;
                        }
                    } elsif( defined($MASTER_STRUCT{$host}{$port}{"tsuccess"})) {
                        $scount += $MASTER_STRUCT{$host}{$port}{"tsuccess"};
                        $total_scount += $MASTER_STRUCT{$host}{$port}{"tsuccess"};
                    }

                    if( defined($MASTER_STRUCT{$host}{$port}{"fail"})) {
                        foreach (keys %{ $MASTER_STRUCT{$host}{$port}{"fail"} }){
                            $ftotal{$_} = 1 unless exists $ftotal{$_};
                            $fcount++;
                            $total_fcount++;
                        }
                    } elsif( defined($MASTER_STRUCT{$host}{$port}{"tfail"})) {
                        $fcount += $MASTER_STRUCT{$host}{$port}{"tfail"};
                        $total_fcount += $MASTER_STRUCT{$host}{$port}{"tfail"};
                    }

                    if( defined($MASTER_STRUCT{$host}{$port}{"error"})) {
                        $ecount++; $total_ecount++;
                        # Commented failure trackers, as general errors aren't the same as test failures.
                        #$fcount++; $total_fcount++;
                    }

                    #if( $scount == 0 && $fcount == 0 ) { $ecount++; $total_ecount++; }

                    unless( $NPStatus::Init::report_summary ) {
                        print "$host:$port: ";
                        if( $ecount > 0 ) {
                            print "err: " . $MASTER_STRUCT{$host}{$port}{"error"} . "\n";
                        } elsif( $fcount > 0 ) {
                            print "$fcount failures and $scount successes. ";
                            if( defined($MASTER_STRUCT{$host}{$port}{"appversion"}) ) {
                                print "version: " . $MASTER_STRUCT{$host}{$port}{"appversion"} . " ";
                            }
                            if( defined($MASTER_STRUCT{$host}{$port}{"uptime"}) ) {
                                print "uptime: " . $MASTER_STRUCT{$host}{$port}{"uptime"} . " ";
                            }
                            print "\n";
                        } else {
                            print "$scount successes. ";
                            if( defined($MASTER_STRUCT{$host}{$port}{"appversion"}) ) {
                                print "version: " . $MASTER_STRUCT{$host}{$port}{"appversion"} . " ";
                            }
                            if( defined($MASTER_STRUCT{$host}{$port}{"uptime"}) ) {
                                print "uptime: " . $MASTER_STRUCT{$host}{$port}{"uptime"} . " ";
                            }
                            print "\n";
                        }
                    }
                }
            }

            if( $NPStatus::Init::report_summary ) {

                # Print the summary
                printf "%-4d failures  %-4d successes  %-4d general errors\n", $total_fcount, $total_scount, $total_ecount;
            }
            else {
                print "-" x 94 . "\n";
                my $report_string = 'There were ';
                if( $total_fcount >= 0 ) {
                    $report_string .= $total_fcount . " test failures. ";
                }
                if( $total_ecount > 0 ) {
                    $report_string .= $total_ecount . " errors. ";
                }
                if( $total_scount >= 0 ) {
                    $report_string .= $total_scount . " successes. ";
                }
                print $report_string . "\n\n";

                if( keys %ftotal) {
                    print "Failures:\n";
                    foreach( sort keys %ftotal ) { print "\t" . $_ . "\n";}
                }
                if( keys %stotal) {
                    print "Successes:\n";
                    foreach( sort keys %stotal ) { print "\t" . $_ . "\n";}
                }
                print "\n";
            }
        }
        else {

            # Print a summary only (LEGACY code)

            print "\n";
            print "-" x 94 . "\n";
            print "There were $totalsuccess successes and $totalfailure failures for $NPStatus::Init::service_name\n\n";

            unless( $NPStatus::Init::report_summary ) {
                if ( $totalsuccess == 0 ) {
                    print "WARNING: total successes is ZERO!\n";
                }
                if ( $totalsuccess > 0 ) {
                    printf "%6s  %s\n", "Count", "Successful Check";
                    print "-" x 40 . "\n";

                    foreach my $key ( sort keys %SUCCESS ) {
                        my $value = $SUCCESS{$key};
                        printf "%4d) %s\n", $value, $key;

                    }
                    print "\n";

                }
                if ( $totalfailure > 0 ) {
                    print "--------------------------------------------------------\n";
                    printf "%6s  %s\n", "Count", "Failed Check";

                    print "-" x 40 . "\n";

                    foreach my $key ( sort keys %CONNECTIONS ) {
                        my $value = $CONNECTIONS{$key};
                        printf "%4d) %s\n", $value, $key;

                    }
                    foreach my $key ( sort keys %UPDATES ) {
                        my $value = $UPDATES{$key};
                        printf "%4d) %s\n", $value, $key;

                    }
                    foreach my $key ( sort keys %UNKNOWN ) {
                        my $value = $UNKNOWN{$key};
                        printf "%4d) %s\n", $value, $key;

                    }
                    print "\n--------------------------------------------------------\n";

                }

                $End = Timestamp();
                print "Start:$Begin Finish:$End\n";
            }
        }
    }
    elsif( $output_format eq 'json' ) {

        use JSON;
        print encode_json( \%MASTER_STRUCT );
    }
}

# Check Result
sub parseCommandResult() {

    my ($tier, $host, $port, $service_name, @cmdresult) = @_;
    my $failure = 0;
    my $success = 0;
    my %return_SUCCESS;
    my %return_FAILS;
    my $appversion = '';
    my $uptime = '';
    my $error_text = '';
    my $appversionset = 0;
    my $switch_on = 0;
    my $preline = '';

    # Parsing is different based on the test types being ran, so split them up for now.
    if( $NPStatus::Init::test_type eq "scrape" || $NPStatus::Init::test_type eq "intvip" || $NPStatus::Init::test_type eq "extvip" ) {

        if( $NPStatus::Services::services->{$service_name}->{$tier} eq 'tomcat' ) {
            my $api_countries_check = 0;
            my $api_countriesUS_check = 0;
            my $exhibitor_state = 0;
            my $zkeeper_client_connection_count = 0;
            my $mqp_state = 0;
            my @vsplit = ();

            foreach my $line (@cmdresult) {

                if ( $line =~ m/curl: \(28\)/ ) {
                    $error_text = "JVM not serving requests: Timeout (" . $NPStatus::Init::global_timeout . "s)";
                    last;
                }
                elsif ( $line =~ m/curl: \(7\)/ ) {
                    $error_text = "Service port not open: Couldn't connect";
                    last;
                }

                if( $service_name eq "accounts" ) {

                    if ( $line =~ m/class="success"/ ) {
                        $success++;
                        $totalsuccess++;
                        ( $service, $status ) = split("</td><td class", $line);
                        $service =~ s/&nbsp;//g;
                        $service =~ s/^\s+//g;
                        $service =~ s/\<td\>//g;
                        !defined( $SUCCESS{$service} ) ? $SUCCESS{$service} = 1 : $SUCCESS{$service}++;
                        !defined( $return_SUCCESS{$service} ) ? $return_SUCCESS{$service} = 1 : $return_SUCCESS{$service}++;
                    }
                    elsif ( $line =~ m/class="fail"/ ) {
                        $failure++;
                        $totalfailure++;
                        ( $service, $status ) = split("</td><td class", $line);
                        $service =~ s/&nbsp;//g;
                        $service =~ s/^\s+//g;
                        $service =~ s/\<td\>//g;
                        !defined( $UNKNOWN{$service} ) ? $UNKNOWN{$service} = 1 : $UNKNOWN{$service}++;
                        !defined( $return_FAILS{$service} ) ? $return_FAILS{$service} = 1 : $return_FAILS{$service}++;
                    }
                    # Get version number if it's printed
                    elsif( !$appversionset && ($line =~ m/VCS-Version/ || $line =~ m/build-version/ || $line =~ m/app.version/) ) {
                        @vsplit = split(/td/, $line);
                        $appversion = $vsplit[3];
                        if( $appversion =~ m/unknown/ ) {
                            $appversion = '';
                            next;
                        }
                        ($appversion) = ($appversion =~ /([0-9]+\.[A-Z0-9.]+)/);
                        $appversionset = 1;
                    }
                    elsif ( $line =~ m/^\|\/api\/v1\/countries\|/ ) {
                        NPStatus::Core::debug( "switch_on a for line $line\n" );
                        $switch_on++;
                        $api_countries_check++;
                        next;
                    }
                    elsif ( $line =~ m/^\|\/api\/v1\/countries\/US\|/ ) {
                        NPStatus::Core::debug( "switch_on b for line $line\n" );
                        $switch_on++;
                        $api_countriesUS_check++;
                        next;
                    }
                    elsif( $switch_on ){

                        if( $api_countries_check ) {
                            if( $line =~ m/^\|end/ ) {
                                NPStatus::Core::debug( "reached the end\n" );
                                $switch_on = 0;
                                $api_countries_check = 0;

                                # if we don't have any successes, then mark this check as failed
                                if( !defined( $return_SUCCESS{"/api/v1/countries"} )) {
                                    NPStatus::Core::debug( "setting fail for /api/v1/countries on line $line\n" );
                                    !defined( $UNKNOWN{"/api/v1/countries"} ) ? $UNKNOWN{"/api/v1/countries"} = 1 : $UNKNOWN{"/api/v1/countries"}++;
                                    !defined( $return_FAILS{"/api/v1/countries"} ) ? $return_FAILS{"/api/v1/countries"} = 1 : $return_FAILS{"/api/v1/countries"}++;
                                    $failure++;
                                    $totalfailure++;
                                }

                                next;
                            }
                            elsif( $line =~ m/^HTTP\/1\.1 200 OK/ ) {
                                NPStatus::Core::debug( "setting success for /api/v1/countries on line $line\n" );
                                !defined( $SUCCESS{"/api/v1/countries"} ) ? $SUCCESS{"/api/v1/countries"} = 1 : $SUCCESS{"/api/v1/countries"}++;
                                !defined( $return_SUCCESS{"/api/v1/countries"} ) ? $return_SUCCESS{"/api/v1/countries"} = 1 : $return_SUCCESS{"/api/v1/countries"}++;
                                $success++;
                                $totalsuccess++;
                            }
                        }
                        elsif( $api_countriesUS_check ) {
                            if( $line =~ m/^\|end/ ) {
                                NPStatus::Core::debug( "reached the end\n" );
                                $switch_on = 0;
                                $api_countriesUS_check = 0;

                                # if we don't have any successes, then mark this check as failed
                                if( !defined( $return_SUCCESS{"/api/v1/countries/US"} )) {
                                    NPStatus::Core::debug( "setting fail for /api/v1/countries/US on line $line\n" );
                                    !defined( $UNKNOWN{"/api/v1/countries/US"} ) ? $UNKNOWN{"/api/v1/countries/US"} = 1 : $UNKNOWN{"/api/v1/countries/US"}++;
                                    !defined( $return_FAILS{"/api/v1/countries/US"} ) ? $return_FAILS{"/api/v1/countries/US"} = 1 : $return_FAILS{"/api/v1/countries/US"}++;
                                    $failure++;
                                    $totalfailure++;
                                }

                                next;
                            }
                            elsif( $line =~ m/^HTTP\/1\.1 200 OK/ ) {
                                NPStatus::Core::debug( "setting success for /api/v1/countries/US on line $line\n" );
                                !defined( $SUCCESS{"/api/v1/countries/US"} ) ? $SUCCESS{"/api/v1/countries/US"} = 1 : $SUCCESS{"/api/v1/countries/US"}++;
                                !defined( $return_SUCCESS{"/api/v1/countries/US"} ) ? $return_SUCCESS{"/api/v1/countries/US"} = 1 : $return_SUCCESS{"/api/v1/countries/US"}++;
                                $success++;
                                $totalsuccess++;
                            }
                        }
                    }
                } elsif( $service_name eq "zkeeper" ) {

                    # Get version number if it's printed
                    if( !$appversionset && ($line =~ m/version/)) {
                        @vsplit = split(",", $line);
                        $appversion = $vsplit[0];
                        ($appversion) = ($appversion =~ /([0-9]+\.[A-Z0-9.-]+)/);
                        $appversionset = 1;
                    }
                    elsif ( $line =~ m/^\|exhibitor-state\|/ ) {
                        NPStatus::Core::debug( "switch_on a for line $line\n" );
                        $switch_on++;
                        $exhibitor_state++;
                        next;
                    }
                    elsif ( $line =~ m/^\|zkeeper-client-connection-count\|/ ) {
                        NPStatus::Core::debug( "switch_on a for line $line\n" );
                        $switch_on++;
                        $zkeeper_client_connection_count++;
                        next;
                    }
                    elsif( $switch_on ){

                        if( $zkeeper_client_connection_count ) {
                            if( $line =~ m/^\|end/ ) {
                                NPStatus::Core::debug( "reached the end\n" );
                                $switch_on = 0;
                                $zkeeper_client_connection_count = 0;

                                # if we don't have any successes, then mark this check as failed
                                if( !defined( $return_SUCCESS{"zkeeper-client-connection-count"} )) {
                                    NPStatus::Core::debug( "setting fail for zkeeper-client-connection-count on line $line\n" );
                                    !defined( $UNKNOWN{"zkeeper-client-connection-count"} ) ? $UNKNOWN{"zkeeper-client-connection-count"} = 1 : $UNKNOWN{"zkeeper-client-connection-count"}++;
                                    !defined( $return_FAILS{"zkeeper-client-connection-count"} ) ? $return_FAILS{"zkeeper-client-connection-count"} = 1 : $return_FAILS{"zkeeper-client-connection-count"}++;
                                    $failure++;
                                    $totalfailure++;
                                }

                                next;
                            }
                            elsif( $line =~ m/^\s\/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:[0-9]+/ ) {
                                NPStatus::Core::debug( "setting success for zkeeper-client-connection-count on line $line\n" );
                                !defined( $SUCCESS{"zkeeper-client-connection-count"} ) ? $SUCCESS{"zkeeper-client-connection-count"} = 1 : $SUCCESS{"zkeeper-client-connection-count"}++;
                                !defined( $return_SUCCESS{"zkeeper-client-connection-count"} ) ? $return_SUCCESS{"zkeeper-client-connection-count"} = 1 : $return_SUCCESS{"zkeeper-client-connection-count"}++;
                                $success++;
                                $totalsuccess++;
                            }
                        } elsif( $exhibitor_state ) {
                            if( $line =~ m/^\|end/ ) {
                                NPStatus::Core::debug( "reached the end\n" );
                                $switch_on = 0;
                                $exhibitor_state = 0;

                                # if we don't have any successes, then mark this check as failed
                                if( !defined( $return_SUCCESS{"exhibitor-state"} )) {
                                    NPStatus::Core::debug( "setting fail for exhibitor-state on line $line\n" );
                                    !defined( $UNKNOWN{"exhibitor-state"} ) ? $UNKNOWN{"exhibitor-state"} = 1 : $UNKNOWN{"exhibitor-state"}++;
                                    !defined( $return_FAILS{"exhibitor-state"} ) ? $return_FAILS{"exhibitor-state"} = 1 : $return_FAILS{"exhibitor-state"}++;
                                    $failure++;
                                    $totalfailure++;
                                }

                                next;
                            }
                            elsif( $line =~ m/\"description\":\"serving\"/ ) {
                                NPStatus::Core::debug( "setting success for exhibitor-state on line $line\n" );
                                !defined( $SUCCESS{"exhibitor-state"} ) ? $SUCCESS{"exhibitor-state"} = 1 : $SUCCESS{"exhibitor-state"}++;
                                !defined( $return_SUCCESS{"exhibitor-state"} ) ? $return_SUCCESS{"exhibitor-state"} = 1 : $return_SUCCESS{"exhibitor-state"}++;
                                $success++;
                                $totalsuccess++;
                            }
                        }
                    }
                } elsif( $service_name eq "mqp" ) {

                    # Get version number if it's printed
                    # if( !$appversionset && ($line =~ m/BrokerVersion/)) {
                    #     @vsplit = split("\"BrokerVersion\":", $line);
                    #     $appversion = $vsplit[0];
                    #     ($appversion) = ($appversion =~ /\"([0-9]+\.[0-9.]+[0-9]+)\"/);
                    #     $appversionset = 1;
                    # }
                    if ( $line =~ m/^\|mqp-state\|/ ) {
                        NPStatus::Core::debug( "switch_on a for line $line\n" );
                        $switch_on++;
                        $mqp_state++;
                        next;
                    }
                    elsif( $switch_on ){

                        if( $mqp_state ) {
                            if( $line =~ m/^\|end/ ) {
                                NPStatus::Core::debug( "reached the end\n" );
                                $switch_on = 0;
                                $mqp_state = 0;

                                # If this is a master host, but master not responding, mark failed
                                # If this is a master host, and quorum check failed, mark failed
                                if( !defined($return_SUCCESS{'mqp-state'}) ) {
                                    NPStatus::Core::debug( "setting fail for mqp-state on line $line\n" );
                                    !defined( $UNKNOWN{"mqp-state"} ) ? $UNKNOWN{"mqp-state"} = 1 : $UNKNOWN{"mqp-state"}++;
                                    !defined( $return_FAILS{"mqp-state"} ) ? $return_FAILS{"mqp-state"} = 1 : $return_FAILS{"mqp-state"}++;
                                    $failure++;
                                    $totalfailure++;
                                }
                                next;
                            }
                            elsif( $line =~ m/^HTTP\/1\.1 200 OK/ ) {
                                NPStatus::Core::debug( "setting success for mqp-state on line $line\n" );
                                !defined( $SUCCESS{"mqp-state"} ) ? $SUCCESS{"mqp-state"} = 1 : $SUCCESS{"mqp-state"}++;
                                !defined( $return_SUCCESS{"mqp-state"} ) ? $return_SUCCESS{"mqp-state"} = 1 : $return_SUCCESS{"mqp-state"}++;
                                $success++;
                                $totalsuccess++;
                            }
                            elsif( $line =~ m/\(7\) couldn\'t connect/ ) {
                                NPStatus::Core::debug( "setting success for mqp-state on line $line\n" );
                                !defined( $SUCCESS{"mqp-state"} ) ? $SUCCESS{"mqp-state"} = 1 : $SUCCESS{"mqp-state"}++;
                                !defined( $return_SUCCESS{"mqp-state"} ) ? $return_SUCCESS{"mqp-state"} = 1 : $return_SUCCESS{"mqp-state"}++;
                                $success++;
                                $totalsuccess++;
                            }
                            elsif( $line =~ m/\"Status\":\"[12]/ ) {
                                NPStatus::Core::debug( "setting success for qourum-state on line $line\n" );
                                !defined( $SUCCESS{"mqp-state"} ) ? $SUCCESS{"mqp-state"} = 1 : $SUCCESS{"mqp-state"}++;
                                !defined( $return_SUCCESS{"mqp-state"} ) ? $return_SUCCESS{"mqp-state"} = 1 : $return_SUCCESS{"mqp-state"}++;
                                $success++;
                                $totalsuccess++;
                            }
                        }
                    }
                } else {

                    if ( $line =~ m/class="success"/ ) {
                        if ( $service_name eq "act" ) {    #ACT is different
                            ( $succeeded,  $v1 )     = split( "\\\td>", $preline );
                            ( $service, $status ) = split( "\&nbsp", $succeeded );
                            $service =~ s/<\/?td>//g;
                            $service =~ s/^\s+//g;
                            $service =~ s/\s+$//g;
                        }
                        else {
                            ($service, $status) = split("</td><td class", $line);
                            $service =~ s/&nbsp;//g;
                            $service =~ s/^\s+//g;
                            $service =~ s/\<td\>//g;
                        }
                        NPStatus::Core::debug( "Parsed SUCCESS for '" . $service ."'\n" );
                        !defined( $SUCCESS{$service} ) ? $SUCCESS{$service} = 1 : $SUCCESS{$service}++;
                        !defined( $return_SUCCESS{$service} ) ? $return_SUCCESS{$service} = 1 : $return_SUCCESS{$service}++;
                        $success++;
                        $totalsuccess++;
                    }
                    elsif ( $line =~ m/class="fail"/ ) {
                        if ( $service_name eq "act" ) {    #ACT is different
                            ( $failed,  $v1 )     = split( "\\\td>", $preline );
                            ( $service, $status ) = split( "\&nbsp", $failed );
                            $service =~ s/<\/?td>//g;
                            $service =~ s/^\s+//g;
                            $service =~ s/\s+$//g;
                        }
                        else {
                            ($service, $status) = split("</td><td class", $line);
                            $service =~ s/&nbsp;//g;
                            $service =~ s/^\s+//g;
                            $service =~ s/\<td\>//g;
                        }
                        !defined( $UNKNOWN{$service} ) ? $UNKNOWN{$service} = 1 : $UNKNOWN{$service}++;
                        !defined( $return_FAILS{$service} ) ? $return_FAILS{$service} = 1 : $return_FAILS{$service}++;
                        NPStatus::Core::debug( "Parsed FAIL for '" . $service ."'\n" );
                        $failure++;
                        $totalfailure++;
                    }
                    # Get version number if it's printed
                    elsif( !$appversionset && ($line =~ m/VCS-Version/ || $line =~ m/build-version/ || $line =~ m/app.version/) ) {
                        my @split = split(/td/, $line);
                        $appversion = $split[3];
                        if( $appversion =~ m/unknown/ ) {
                            $appversion = '';
                            next;
                        }
                        ($appversion) = ($appversion =~ /([0-9]+\.[A-Z0-9.]+)/);
                        $appversionset = 1;
                    }

                    # Get uptime value if available
                    elsif( $line =~ m/UpTimeMinutes/ ) {
                        $line =~ s/(&[^;]+;|<\/td>)//g;
                        my @split = split(/<td>/, $line);
                        $uptime = $split[2];
                        !defined( $SUCCESS{uptime} ) ? $SUCCESS{uptime} = 1 : $SUCCESS{uptime}++;
                        !defined( $return_SUCCESS{uptime} ) ? $return_SUCCESS{uptime} = 1 : $return_SUCCESS{uptime}++;
                        $success++;
                        $totalsuccess++;
                    }
                }
                # assign current line to previous line var. This is for ACT report parsing since it's HTML is format differently.
                $preline = $line;
            }
        } elsif( $NPStatus::Services::services->{$service_name}->{$tier} eq 'apache' ) {

            if( $service_name eq "consolegw" ) {
                foreach my $line (@cmdresult) {

                    if( $line =~ m/CyberArk_Key_Loaded/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/All_Loaded_Successfully/ ) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }
                    if( $line =~ m/CGW_Initialize_Running/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/true/) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }
                    if( $line =~ m/CommerceServerConnectionStatus/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/true/) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }
                    if( $line =~ m/AASServerConnectionStatus/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/true/) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }
                    if( $line =~ m/PDSServerConnectionStatus/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/true/) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }
                    if( $line =~ m/ConsoleNotificationEnable/ ) {

                        $line =~ s/^\s+//;
                        $line =~ s/[",]+//g;
                        $line =~ s/:\s+/:/;
                        my @pair = split(":", $line);
                        NPStatus::Core::debug( "cgw key: '" . $pair[0] . "', val: '" . $pair[1] ."'\n" );
                        if( $pair[1] =~ m/true/) {
                            !defined( $SUCCESS{$pair[0]} ) ? $SUCCESS{$pair[0]} = 1 : $SUCCESS{$pair[0]}++;
                            !defined( $return_SUCCESS{$pair[0]} ) ? $return_SUCCESS{$pair[0]} = 1 : $return_SUCCESS{$pair[0]}++;
                            $success++;
                            $totalsuccess++;
                        } else {
                            !defined( $UNKNOWN{$pair[0]} ) ? $UNKNOWN{$pair[0]} = 1 : $UNKNOWN{$pair[0]}++;
                            !defined( $return_FAILS{$pair[0]} ) ? $return_FAILS{$pair[0]} = 1 : $return_FAILS{$pair[0]}++;
                            $failure++;
                            $totalfailure++;
                        }
                    }

                    # Get version number if it's printed
                    if( !$appversionset && $line =~ m/CGW_Build_Version/ ) {
                        NPStatus::Core::debug( "detected\n" );
                        my @split = split(":", $line);
                        $appversion = $split[1];
                        $appversion =~ s/\s+//m;
                        $appversion =~ s/,//m;
                        $appversion =~ s/"//g;
                        @split = split(/\s/,$appversion);
                        $appversion = $split[0];
                        $appversionset = 1;
                    }
                }
            } else {

                # By default any service that has tier 1 as apache should be running modjk, so parse test results here.
                # Otherwise, the app should have special parsing and handling above.

                use XML::Simple;

                # Get the name of the load balancer, so we can parse the XML output below
                my $balancers = $NPStatus::Services::services->{$service_name}->{modjk_group};
                my $balancer_name = "";
                foreach my $balancer (@$balancers) {
                    my @names = split(":", $balancer);
                    if( $names[1] == $port ) {
                        $balancer_name = $names[0];
                        last;
                    }
                }

                # Convert XML to hash
                my $cmdresultstr = join("", @cmdresult);
                $cmdresultstr =~ s/.+<\?xml/<?xml/gs;
                $cmdresultstr =~ s/<\/jk:status>.*/<\/jk:status>/gs;

                my $status = XMLin(join("", $cmdresultstr), forcearray => ['jk:member', 'jk:balancer']);
                my $balancer = $status->{'jk:balancers'}->{'jk:balancer'}->{$balancer_name};

                # Test whether all members of the modjk pool are healthy
                if( $balancer->{good} == $balancer->{member_count} ) {
                    !defined( $SUCCESS{modjk_pool_state} ) ? $SUCCESS{modjk_pool_state} = 1 : $SUCCESS{modjk_pool_state}++;
                    !defined( $return_SUCCESS{modjk_pool_state} ) ? $return_SUCCESS{modjk_pool_state} = 1 : $return_SUCCESS{modjk_pool_state}++;
                    $success++;
                    $totalsuccess++;
                } else {
                    !defined( $UNKNOWN{modjk_pool_state} ) ? $UNKNOWN{modjk_pool_state} = 1 : $UNKNOWN{modjk_pool_state}++;
                    !defined( $return_FAILS{modjk_pool_state} ) ? $return_FAILS{modjk_pool_state} = 1 : $return_FAILS{modjk_pool_state}++;
                    $failure++;
                    $totalfailure++;
                }

                # Test activation state - should be all in ACT state
                my $bad_activation = 0;
                my $bad_worker_state = 0;
                for my $member (keys %{$balancer->{'jk:member'}}) {
                    my $memberobj = $balancer->{'jk:member'}->{$member};
                    if( $memberobj->{'activation'} ne 'ACT' ) { $bad_activation++; }
                    if( $memberobj->{'state'} ne 'OK' && $memberobj->{'state'} ne 'OK/IDLE' )  { $bad_worker_state++; }
                }
                if ($bad_activation > 0) {
                    !defined( $UNKNOWN{modjk_worker_activation_state} ) ? $UNKNOWN{modjk_worker_activation_state} = 1 : $UNKNOWN{modjk_worker_activation_state}++;
                    !defined( $return_FAILS{modjk_worker_activation_state} ) ? $return_FAILS{modjk_worker_activation_state} = 1 : $return_FAILS{modjk_worker_activation_state}++;
                    $failure++;
                    $totalfailure++;
                } else {
                    !defined( $SUCCESS{modjk_worker_activation_state} ) ? $SUCCESS{modjk_worker_activation_state} = 1 : $SUCCESS{modjk_worker_activation_state}++;
                    !defined( $return_SUCCESS{modjk_worker_activation_state} ) ? $return_SUCCESS{modjk_worker_activation_state} = 1 : $return_SUCCESS{modjk_worker_activation_state}++;
                    $success++;
                    $totalsuccess++;
                }
                if ($bad_worker_state > 0) {
                    !defined( $UNKNOWN{modjk_worker_state} ) ? $UNKNOWN{modjk_worker_state} = 1 : $UNKNOWN{modjk_worker_state}++;
                    !defined( $return_FAILS{modjk_worker_state} ) ? $return_FAILS{modjk_worker_state} = 1 : $return_FAILS{modjk_worker_state}++;
                    $failure++;
                    $totalfailure++;
                } else {
                    !defined( $SUCCESS{modjk_worker_state} ) ? $SUCCESS{modjk_worker_state} = 1 : $SUCCESS{modjk_worker_state}++;
                    !defined( $return_SUCCESS{modjk_worker_state} ) ? $return_SUCCESS{modjk_worker_state} = 1 : $return_SUCCESS{modjk_worker_state}++;
                    $success++;
                    $totalsuccess++;
                }

                #NPStatus::Core::debug( "jk member count " . $member_count ."\n" );
            }
        }

        if ( $success == 0 && $failure == 0 && length($error_text) == 0 ) {
            if( $service_name eq "consolegw" ) { $error_text = "Service not running"; }
            else { $error_text = "JVM not serving requests (reason unknown)"; }
        }

    } elsif( $NPStatus::Init::test_type eq "system" ) {

        my $splunkcheck = 0;
        my $cyberarkcheck = 0;
        my $introscope = 0;

        foreach my $line (@cmdresult) {

            if( $switch_on ){
                if( $introscope ) {
                    if( $line =~ m/^\|end/ ) {
                        $switch_on--;
                        $introscope--;

                        # if we don't have any successes, then mark this check as failed
                        if( !defined( $return_SUCCESS{introscope} )) {
                            NPStatus::Core::debug( "setting fail on line $line\n" );
                            !defined( $UNKNOWN{introscope} ) ? $UNKNOWN{introscope} = 1 : $UNKNOWN{introscope}++;
                            !defined( $return_FAILS{introscope} ) ? $return_FAILS{introscope} = 1 : $return_FAILS{introscope}++;
                            $failure++;
                            $totalfailure++;
                        }

                        next;
                    }
                    elsif( $line =~ /introscope/ ) {
                        NPStatus::Core::debug( "setting success on line $line\n" );
                        !defined( $SUCCESS{introscope} ) ? $SUCCESS{introscope} = 1 : $SUCCESS{introscope}++;
                        !defined( $return_SUCCESS{introscope} ) ? $return_SUCCESS{introscope} = 1 : $return_SUCCESS{introscope}++;
                        $success++;
                        $totalsuccess++;
                    }
                }


                elsif( $splunkcheck ) {
                    if( $line =~ m/^\|end/ ) {
                        $switch_on--;
                        $splunkcheck--;

                        # if we don't have any successes, then mark this check as failed
                        if( !defined( $return_SUCCESS{splunkd} )) {
                            NPStatus::Core::debug( "setting fail on line $line\n" );
                            !defined( $UNKNOWN{splunkd} ) ? $UNKNOWN{splunkd} = 1 : $UNKNOWN{splunkd}++;
                            !defined( $return_FAILS{splunkd} ) ? $return_FAILS{splunkd} = 1 : $return_FAILS{splunkd}++;
                            $failure++;
                            $totalfailure++;
                        }

                        next;
                    }
                    elsif( $line =~ m/^splunkd/ ) {
                        NPStatus::Core::debug( "setting success on line $line\n" );
                        !defined( $SUCCESS{splunkd} ) ? $SUCCESS{splunkd} = 1 : $SUCCESS{splunkd}++;
                        !defined( $return_SUCCESS{splunkd} ) ? $return_SUCCESS{splunkd} = 1 : $return_SUCCESS{splunkd}++;
                        $success++;
                        $totalsuccess++;
                    }
                }
                elsif( $cyberarkcheck ) {
                    if( $line =~ m/^\|end/ ) {
                        $switch_on--;
                        $cyberarkcheck--;

                        # if we don't have any successes, then mark this check as failed
                        if( !defined( $return_SUCCESS{cyberark} )) {
                            NPStatus::Core::debug( "setting fail on line $line\n" );
                            !defined( $UNKNOWN{cyberark} ) ? $UNKNOWN{cyberark} = 1 : $UNKNOWN{cyberark}++;
                            !defined( $return_FAILS{cyberark} ) ? $return_FAILS{cyberark} = 1 : $return_FAILS{cyberark}++;
                            $failure++;
                            $totalfailure++;
                        }

                        next;
                    }
                    elsif( $line =~ m/appprovider\s-mode\sSERVICE/ ) {
                        NPStatus::Core::debug( "setting success on line $line\n" );
                        !defined( $SUCCESS{cyberark} ) ? $SUCCESS{cyberark} = 1 : $SUCCESS{cyberark}++;
                        !defined( $return_SUCCESS{cyberark} ) ? $return_SUCCESS{cyberark} = 1 : $return_SUCCESS{cyberark}++;
                        $success++;
                        $totalsuccess++;
                    }
                }
            }

            if ( $line =~ m/^\|splunkdcheck/ ) {
                NPStatus::Core::debug( "switch_on for line $line\n" );
                $switch_on++;
                $splunkcheck++;
                next;
            }
            elsif ( $line =~ m/^\|introscope/ ) {
                NPStatus::Core::debug( "switch_on for line $line\n" );
                $switch_on++;
                $introscope++;
                next;
            }
            elsif ( $line =~ m/^\|cyberarkcheck/ ) {
                NPStatus::Core::debug( "switch_on for line $line\n" );
                $switch_on++;
                $cyberarkcheck++;
                next;
            }
        }
    }

    NPStatus::Core::debug( "Report \%SUCCESS: " . %return_SUCCESS . "\n" );
    NPStatus::Core::debug( "Report \%FAILURE: " . %return_FAILS . "\n" );
    NPStatus::Core::debug( "Report \%success: " . $success . "\n" );
    NPStatus::Core::debug( "Report \%failure: " . $failure . "\n" );
    NPStatus::Core::debug( "Report \$appversion: " . $appversion . "\n" );
    NPStatus::Core::debug( "Report \$uptime: " . $uptime . "\n" );
    return ( \%return_SUCCESS, \%return_FAILS, $success, $failure, $appversion, $error_text, $uptime );
}

sub parallelAggregate {

    my $kidpids = shift;
    my @kidpids = @$kidpids;
    my @MainPortResultArray;
    %MASTER_STRUCT = ();


    foreach my $kidpid (@kidpids) {

        # This is where we create the data structures for report generation
        # These structures are created, for now, from flat files that each child writes to the filesystem which contain their test results

        NPStatus::Core::debug( "aggregating stats for kid $kidpid for test type " . $NPStatus::Init::test_type . "\n" );

        # The method for parsing differs based on the tests ran, for now we need to split them up
        if( $NPStatus::Init::test_type eq "scrape" || $NPStatus::Init::test_type eq "intvip" || $NPStatus::Init::test_type eq "extvip" ) {

            my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $kidpid;
            my (%success, %fail, $tsuccess, $tfail);

            if( $NPStatus::Init::raw_report ){

                # If the user has set --raw flag, just dump out the status page, do not parse
                open( FILE, "< $child_shm_path" );
                while( <FILE> ) { print $_ }
                close( FILE );
                next;
            }

            #### NEW WAY
            $totalsuccess = 0;
            $totalfailure = 0;
            my $service_name = $NPStatus::Init::service_name;
            my $port = '';
            my %PortDetailHash;
            my %PortResultHash;
            my @success_array;
            my @fail_array;
            my $hostname = '';
            my $error_text = '';
            my $appversion = '';
            my $uptime = '';
            my $portstatus = 1;

            open( FILE, "< $child_shm_path" );

            while( <FILE> ) {

                my @data = split( /[|]/, $_);
                foreach (@data) { NPStatus::Core::debug( 2, "split line: " . $_ . "\n") }
                chomp($data[1]);
                # Old way, just so it works
                if( $_ =~ m/^success/ ) {
                    chomp( $data[2] );
                    $success{$data[1]} = $data[2];
                }
                elsif( $_ =~ m/^fail/ ) {
                    chomp( $data[2] );
                    $fail{$data[1]} = $data[2];
                }
                elsif( $_ =~ m/^tsuccess/ ) {
                    $totalsuccess += $data[1];
                }
                elsif( $_ =~ m/^appversion/ ) {
                    $appversion = $data[1];
                }
                elsif( $_ =~ m/^uptime/ ) {
                    $uptime = $data[1];
                }
                elsif( $_ =~ m/^tfail/ ) {
                    $totalfailure += $data[1];
                }
                elsif( $_ =~ m/^err/ ) {
                    $error_text = $data[1];
                }
                elsif( $_ =~ m/^host/ ) {
                    $hostname = $data[1];
                }
                elsif( $_ =~ m/^port/ ) {
                    $port = $data[1];
                }

                ### NEW WAY

                # print FILE "host:" . $host . "\n";
                # print FILE "port:" . $port . "\n";
                # print FILE "tsuccess:" . $totalsuccess . "\n";
                # print FILE "tfail:" . $totalfail . "\n";
                # print FILE "service_name:" . $service_name . "\n";
                # print FILE "err:" . $error_text . "\n" if defined($error_text);

                if( $_ =~ m/hostname/ ) {
                    $hostname = $data[1];
                }
                elsif( $_ =~ m/^port/ ){
                    $port = $data[1];
                }
                elsif( $_ =~ m/^success/ ) {
                    #$DetailHash{"success"} = [];
                    push( @success_array, $data[1] );
                    $tsuccess++;
                }
                elsif( $_ =~ m/^fail/ ) {
                    push( @fail_array, $data[1] );
                    $tfail++;
                    $portstatus = 0;
                }
            }

            # OLD WAY
            # Map values in passed in parameters to NPStatus::Report internal objects so printStatistics() will work
            if( length( $error_text ) > 0 ){
                $MASTER_STRUCT{$hostname}{$port}{"error"} = $error_text;
            }
            if( length( $appversion )) {
                $MASTER_STRUCT{$hostname}{$port}{"appversion"} = $appversion;
            }
            if( length( $uptime )) {
                $MASTER_STRUCT{$hostname}{$port}{"uptime"} = $uptime;
            }
            $MASTER_STRUCT{$hostname}{$port}{"tsuccess"} = $totalsuccess;
            $MASTER_STRUCT{$hostname}{$port}{"tfail"} = $totalfailure;
            foreach my $key (keys %success) {
                unless( defined( $SUCCESS{$key} ) ) {

                    $MASTER_STRUCT{$hostname}{$port}{"success"}{$key} = $success{$key};
                    $SUCCESS{$key} = $success{$key};
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} not defined, value: ". $SUCCESS{$key} . " \n" );
                } else {

                    $MASTER_STRUCT{$hostname}{$port}{"success"}{$key} += $success{$key};
                    $SUCCESS{$key} += $success{$key} ;
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} defined value: ". $SUCCESS{$key} . "\n" );
                }
            }
            foreach my $key (keys %fail) {
                unless( defined( $UNKNOWN{$key} ) ) {

                    $MASTER_STRUCT{$hostname}{$port}{"fail"}{$key} = $success{$key};
                    $UNKNOWN{$key} = $fail{$key};
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} not defined, value: ". $UNKNOWN{$key} . " \n" );
                } else {

                    $MASTER_STRUCT{$hostname}{$port}{"fail"}{$key} += $fail{$key};
                    $UNKNOWN{$key} += $fail{$key} ;
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} defined value: ". $UNKNOWN{$key} . "\n" );
                }
            }

            # NEW WAY
            # Add the newly formed host Hash to the array
            $PortDetailHash{"success"} = [ @success_array ];
            $PortDetailHash{"fail"} = [ @fail_array ];

            $PortResultHash{"port"} = $port;
            $PortResultHash{"status"} = "good" if $portstatus;
            $PortResultHash{"status"} = "bad" unless $portstatus;
            $PortResultHash{"details"} = { %PortDetailHash };
            $PortResultHash{"hostname"} = $hostname;

            push( @MainPortResultArray, { %PortResultHash } );
            close(FILE);
        }
        elsif( $NPStatus::Init::test_type eq "system" ) {

            my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $kidpid;
            my (%success, %fail, $tsuccess, $tfail);

            if( $NPStatus::Init::raw_report ){

                # If the user has set --raw flag, just dump out the status page, do not parse
                open( FILE, "< $child_shm_path" );
                while( <FILE> ) { print $_ }
                close( FILE );
                next;
            }

            #### NEW WAY
            $totalsuccess = 0;
            $totalfailure = 0;
            my $service_name = $NPStatus::Init::service_name;
            my $port = '';
            my %PortDetailHash;
            my %PortResultHash;
            my @success_array;
            my @fail_array;
            my $hostname = '';
            my $error_text = '';
            my $appversion = '';
            my $portstatus = 1;

            open( FILE, "< $child_shm_path" );

            while( <FILE> ) {

                my @data = split( /[|]/, $_);
                foreach (@data) { NPStatus::Core::debug( 2, "split line: " . $_ . "\n") }
                chomp($data[1]);
                # Old way, just so it works
                if( $_ =~ m/^success/ ) {
                    $success{$data[1]} = $data[2];
                }
                elsif( $_ =~ m/^fail/ ) {
                    $fail{$data[1]} = $data[2];
                }
                elsif( $_ =~ m/^tsuccess/ ) {
                    $totalsuccess += $data[1];
                }
                elsif( $_ =~ m/^tfail/ ) {
                    $totalfailure += $data[1];
                }
                elsif( $_ =~ m/^err/ ) {
                    $error_text = $data[1];
                }
                elsif( $_ =~ m/^host/ ) {
                    $hostname = $data[1];
                }
                elsif( $_ =~ m/hostname/ ) {
                    $hostname = $data[1];
                }
                elsif( $_ =~ m/^port/ ){
                    $port = $data[1];
                }
                elsif( $_ =~ m/^success/ ) {
                    #$DetailHash{"success"} = [];
                    push( @success_array, $data[1] );
                    $tsuccess++;
                }
                elsif( $_ =~ m/^fail/ ) {
                    push( @fail_array, $data[1] );
                    $tfail++;
                    $portstatus = 0;
                }
            }

            # OLD WAY
            # Map values in passed in parameters to NPStatus::Report internal objects so printStatistics() will work
            if( length( $error_text ) > 0 ){
                $MASTER_STRUCT{$hostname}{$port}{"error"} = $error_text;
            }
            $MASTER_STRUCT{$hostname}{$port}{"tsuccess"} = $totalsuccess;
            $MASTER_STRUCT{$hostname}{$port}{"tfail"} = $totalfailure;
            foreach my $key (keys %success) {
                unless( defined( $SUCCESS{$key} ) ) {

                    $MASTER_STRUCT{$hostname}{$port}{"success"}{$key} = $success{$key};
                    $SUCCESS{$key} = $success{$key};
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} not defined, value: ". $SUCCESS{$key} . " \n" );
                } else {

                    $MASTER_STRUCT{$hostname}{$port}{"success"}{$key} += $success{$key};
                    $SUCCESS{$key} += $success{$key} ;
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} defined value: ". $SUCCESS{$key} . "\n" );
                }
            }
            foreach my $key (keys %fail) {
                unless( defined( $UNKNOWN{$key} ) ) {

                    $MASTER_STRUCT{$hostname}{$port}{"fail"}{$key} = $success{$key};
                    $UNKNOWN{$key} = $fail{$key};
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} not defined, value: ". $UNKNOWN{$key} . " \n" );
                } else {

                    $MASTER_STRUCT{$hostname}{$port}{"fail"}{$key} += $fail{$key};
                    $UNKNOWN{$key} += $fail{$key} ;
                    NPStatus::Core::debug( 2, "Report::Import: SUCCESS{service_name} defined value: ". $UNKNOWN{$key} . "\n" );
                }
            }

            close(FILE);
        }
    } # foreach kidpid



    # For debug purposes
    #
    # foreach my $result(@MainPortResultArray) {

    #     NPStatus::Core::debug( "MainPortResultArray: '\n" );
    #     my %hash = %$result;
    #     foreach my $key (keys %hash ) {

    #         if( $key eq "details" ) {
    #             foreach my $detail (keys $hash{$key}){ NPStatus::Core::debug( "detail: ".$detail."\n") }
    #         } else {
    #             NPStatus::Core::debug( "key: $key , value: " . $hash{$key} . "\n" );
    #         }
    #     }
    #     NPStatus::Core::debug( "' Done\n" );
    # }
}

1;  # don't forget to return a true value from the file
