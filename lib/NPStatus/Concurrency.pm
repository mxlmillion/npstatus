package NPStatus::Concurrency;

use Parallel::ForkManager;
use NPStatus::Core;


sub execTest {

	my ($service_name, $service_array_ref) = @_;
	my $forknum = NPStatus::Core::setMaxForkedProcs( $service_name );
	my $forkmgr = Parallel::ForkManager->new( $forknum );
	my @kidpids;

	NPStatus::Core::debug( "Doing parallel execution \n" );

	# Now perform the forks
	foreach (@$service_array_ref) {

		my ($tier, $host, $port) = split(":", $_);

		# Forks and save the child pid. Used as a unique id so we can later import test results from child
		my $pid = $forkmgr->start;
		push(@kidpids, $pid);
		$pid && next;

		### CHILD ###
		#$ofh = select STDOUT;
		#$| = 1;
		#select $ofh;

		NPStatus::Core::debug( "Testing service $service_name on " . $NPStatus::Init::line . "-" . $NPStatus::Init::environment . " on " . $tier . ":" . $host . ":" . $port . "\n" );
		NPStatus::Core::testService( $service_name, $tier, $host, $port );

		### TERMINATE CHILD ###
		$forkmgr->finish;
	}

	# If we're using a FIFO here, then all children at this point should be getting results, 
	# or already parsed and waiting on us to open the fifo and read all data
	NPStatus::Core::debug( "Waiting for all children...\n" );
	$forkmgr->wait_all_children();

	return \@kidpids;
}


1;
