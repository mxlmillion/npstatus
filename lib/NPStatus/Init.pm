package NPStatus::Init;

use strict;
use warnings; 
use Getopt::Long;
use Sys::Hostname;
use NPStatus::Core;
use NPStatus::Services;
use Data::Dumper;
use Switch;
use Scalar::Util qw(looks_like_number);



# Command line options
our $test_type          = 'scrape';
our $service_name       = '';
our $line               = 'e1';
our $environment        = 'np';
our $host_range         = '';
our $test_all_services  = '';
our $test_all_unsafe    = '';
our $test_all_safe      = '';
our $debug              = 0;
our $help               = '';
our $report_summary     = 0;
our $list_all           = '';
our $output_format      = 'utf8';
our $parallelize        = 1;
our $max_forked_processes = 30;
our $raw_report         = 0;
our $test_dbfacing      = 0;
our $test_non_dbfacing  = 0;
our $user_group         = "spo";		# We will assume SPO if this is left blank
our $tier				= 0;			# tier 0 - test all tiers
our $poll               = 0;
our $global_timeout		= 10;

# Globals
our $services;
our $hostname 			= hostname;
our $prodBastion        = "example_hostname";
our $nonprodBastion     = "example_hostname";
our $succeed            = 0;
our (%SUCCESS, %CONNECTIONS, %UPDATES, %UNKNOWN );
our $host_range_start   = 0;
our $host_range_end     = 0;
our $host_range_mode    = 0;
our $just               = "";
our $suffix             = "";
our ( $sec, $min, $hr, $day, $mon, $year, $v1 );
our ( $Date, $Begin, $End );
our $rand_childshm      = -d "/dev/shm/" ? "/dev/shm/" : "/tmp/";
our $rand_childshm_dir  = $rand_childshm . int(rand(999999));
our $child_shm_ttl      = 120;      # seconds
our $sshusername        = '';
our $sshpassword        = '';
our $curl_agent_name    = 'npstatus-agent/v1';

our $totalsuccess       = 0;
our $totalfailure       = 0;
our $badones            = 0;
our $parallel_testtotalcount = 0;
our @CONNECTIONS;
our @UPDATES;
our @UNKNOWN;

# Turn on buffer flushing on every write call (not ideal)
my $ofh = select STDOUT;
$| = 1;
select $ofh;

# If no arguments provided, print the help
NPStatus::Core::usage() if ( $#::ARGV == -1 );

exit if ! GetOptions (
    'list|L' => \$list_all,
    'test-type|t=s' => \$test_type, 
    'line|l=s' => \$line,
    'environment|env|e=s' => \$environment,
    'service-name|app-name|s=s' => \$service_name,
    'range|r=s' => \$host_range,
    'all-services|a:s' => \$test_all_services,
    'debug|D+' => \$debug,
    'help|h' => \$help,
    'output-format|format=s', \$output_format,
    'parallel|P' => \$parallelize,
    'test-all|all' => \$test_all_services,
    'test-all-safe|safe' => \$test_all_safe,
    'test-all-unsafe|unsafe' => \$test_all_unsafe,
    'test-dbfacing|dbfacing' => \$test_dbfacing,
    'test-non-dbfacing|non-dbfacing' => \$test_non_dbfacing,
    'summary' => \$report_summary,
    'group|g=s' => \$user_group,
    'raw' => \$raw_report,
    'tier=s' => \$tier,
    'poll' => \$poll,
    'timeout=s' => \$global_timeout
);

# Create the children's shared data folder
mkdir( $rand_childshm_dir ) if( $parallelize );
chmod( 0777, $rand_childshm_dir ) if( $parallelize );

# Setup signal handling
$SIG{'INT'} = sub {

    NPStatus::Report::printStatistics() unless( $parallelize );
    #system( "[ -d " . $rand_childshm_dir . " ] && rm -rf " . $rand_childshm_dir . " 2> /dev/null");
    `stty sane 2> /dev/null`;
    exit 255;
};
$SIG{CHLD} = sub {

    # Update testtotal counter and print an update
    #print @_;
    
    #$NPStatus::Init::parallel_testtotalcount++;
    #print $NPStatus::Init::parallel_testtotalcount . "..."; 
};

sub argInit {

    # Check arguments, before execution
    NPStatus::Core::usage() if ( $help );

    # Validate --tier flag
    if ( looks_like_number($tier) && $tier > 2 ) { print "Only 2 tiers are supported, tier 1 and tier 2: " . $tier . "\n"; exit 255; }
    if ( looks_like_number($tier) && $tier == 0 && $host_range ) { print "Having both host range and all tiers selected is not supported. If host range is desired a tier number is required (e.g. --tier 1)\n"; exit 255;}
    NPStatus::Core::debug("tier: " . $tier . "\n");

    # Define the host ranges
    if ( $host_range ) {

        # Add a "-" at the end if user hasn't, just so user can easily specify range by entering just one number
        # The one number then becomes the host to start with.
        $host_range .= "-" if $host_range !~ /-/;
        my $len = my @args = split("-", $host_range);
        NPStatus::Core::debug( "host range detected: length " . $len . ", range: " . $host_range . "\n" );
        $host_range_start = $args[0];
        if( $len == 2 ){
            $host_range_end = $args[1];
        }
        elsif( $len == 0 ) {
            print "Length must contain at least one value (e.g. 6, 6-10, 6-)\n"; exit 255;
        }
    }

    if ( $test_type eq "scrape" ) {

        if( $environment !~ /(np|pl)/ ) {
            print "Environment can be either 'np' or 'pl'\n";
            exit 255;
        }
    }
    elsif( $test_type eq "extvip" || $test_type eq "intvip" ) {

        if( $line !~ /(p1|p2|h1|h2|e1)/ ){
            print "Line can be one of the following: p1, p2, h1, h2, e1\n"; exit 255;
        }
    }

    # Populate services hash
    NPStatus::Core::debug( "Line is '$line'\n" );
    NPStatus::Services::setServiceProperties();
    if( $test_type eq "scrape" || $test_type eq "system" ) {

        if ( $line =~ /^p1/i ) {
            
            if ( $environment eq "np" ) {
                #navigator platform
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setP1_NPTier1Services($line);
                    NPStatus::Services::setP1_NPTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setP1_NPTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setP1_NPTier2Services($line);
                } else {
                    NPStatus::Services::setP1_NPTier1Services($line);
                    NPStatus::Services::setP1_NPTier2Services($line);
                }
            }
            elsif ( $environment eq "pl" ) {
                #prod-line
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setP1_PLTier1Services($line);
                    NPStatus::Services::setP1_PLTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setP1_PLTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setP1_PLTier2Services($line);
                } else {
                    NPStatus::Services::setP1_PLTier1Services($line);
                    NPStatus::Services::setP1_PLTier2Services($line);
                }
            }
        }
        elsif ( $line =~ /^p2/i ) {

            if ( $environment eq "np" ) {
                #navigator platform
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setP2_NPTier1Services($line);
                    NPStatus::Services::setP2_NPTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setP2_NPTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setP2_NPTier2Services($line);
                } else {
                    NPStatus::Services::setP2_NPTier1Services($line);
                    NPStatus::Services::setP2_NPTier2Services($line);
                }
            }
            elsif ( $environment eq "pl" ) {
                #prod-line
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setP2_PLTier1Services($line);
                    NPStatus::Services::setP2_PLTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setP2_PLTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setP2_PLTier2Services($line);
                } else {
                    NPStatus::Services::setP2_PLTier1Services($line);
                    NPStatus::Services::setP2_PLTier2Services($line);
                }
            }
        }
        elsif ( $line =~ /^h/i ) {
            if ( $environment eq "np" ) {
                NPStatus::Core::debug( "getting h-np services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setH_NPTier1Services($line);
                    NPStatus::Services::setH_NPTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setH_NPTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setH_NPTier2Services($line);
                } else {
                    NPStatus::Services::setH_NPTier1Services($line);
                    NPStatus::Services::setH_NPTier2Services($line);
                }
            }
            elsif ( $environment eq "pl" ) {
                NPStatus::Core::debug( "getting h-pl services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setH_PLTier1Services($line);
                    NPStatus::Services::setH_PLTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setH_PLTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setH_PLTier2Services($line);
                } else {
                    NPStatus::Services::setH_PLTier1Services($line);
                    NPStatus::Services::setH_PLTier2Services($line);
                }
            }
        }
        elsif ( $line =~ /^l/i ) {
            if ( $environment eq "np" ) {
                NPStatus::Core::debug( "getting l-np services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setL_NPTier1Services($line);
                    NPStatus::Services::setL_NPTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setL_NPTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setL_NPTier2Services($line);
                } else {
                    NPStatus::Services::setL_NPTier1Services($line);
                    NPStatus::Services::setL_NPTier2Services($line);
                }
            }
            elsif ( $environment eq "pl" ) {
                NPStatus::Core::debug( "getting l-pl services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setL_PLTier1Services($line);
                    NPStatus::Services::setL_PLTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setL_PLTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setL_PLTier2Services($line);
                } else {
                    NPStatus::Services::setL_PLTier1Services($line);
                    NPStatus::Services::setL_PLTier2Services($line);
                }
            }
        }

	elsif ( $line =~ /^c/i ) {
            if ( $environment eq "np" ) {
                NPStatus::Core::debug( "getting c-np services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setC_NPTier1Services($line);
                    NPStatus::Services::setC_NPTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setC_NPTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setC_NPTier2Services($line);
                } else {
                    NPStatus::Services::setC_NPTier1Services($line);
                    NPStatus::Services::setC_NPTier2Services($line);
                }
            }
            elsif ( $environment eq "pl" ) {
                NPStatus::Core::debug( "getting c-pl services.\n" );
                if ( looks_like_number($tier) && $tier == 0 ) {
                    NPStatus::Services::setC_PLTier1Services($line);
                    NPStatus::Services::setC_PLTier2Services($line);
                } elsif ( looks_like_number($tier) && $tier == 1 ) {
                    NPStatus::Services::setC_PLTier1Services($line);
                } elsif ( looks_like_number($tier) && $tier == 2 ) {
                    NPStatus::Services::setC_PLTier2Services($line);
                } else {
                    NPStatus::Services::setC_PLTier1Services($line);
                    NPStatus::Services::setC_PLTier2Services($line);
                }
            }
        }

        # Every other environment
        elsif ( $environment eq "np" ) {
            NPStatus::Core::debug( "getting non production line NP services.\n" );
            if ( looks_like_number($tier) && $tier == 0 ) {
                NPStatus::Services::setO_NPTier1Services($line);
                NPStatus::Services::setO_NPTier2Services($line);
            } elsif ( looks_like_number($tier) && $tier == 1 ) {
                NPStatus::Services::setO_NPTier1Services($line);
            } elsif ( looks_like_number($tier) && $tier == 2 ) {
                NPStatus::Services::setO_NPTier2Services($line);
            } else {
                NPStatus::Services::setO_NPTier1Services($line);
                NPStatus::Services::setO_NPTier2Services($line);
            }
        }
        elsif ( $environment eq "pl" ) {
            if ( looks_like_number($tier) && $tier == 0 ) {
                NPStatus::Services::setO_PLTier1Services($line);
                NPStatus::Services::setO_PLTier2Services($line);
            } elsif ( looks_like_number($tier) && $tier == 1 ) {
                NPStatus::Services::setO_PLTier1Services($line);
            } elsif ( looks_like_number($tier) && $tier == 2 ) {
                NPStatus::Services::setO_PLTier2Services($line);
            } else {
                NPStatus::Services::setO_PLTier1Services($line);
                NPStatus::Services::setO_PLTier2Services($line);
            }
        }
    }
    elsif( $test_type eq "extvip" ) {

        if ( $line =~ /(h|e)/i ) {
            NPStatus::Services::setExtVip_OServices($line);
        }
        elsif ( $line =~ /p/i ) {
            NPStatus::Services::setExtVip_PServices($line);
        }
        else { print "Only valid lines are H1, H2, P1, P2, e1\n"; exit 255; }
    }
    elsif ( $test_type eq "intvip" ) {

        if ( $line =~ /(e|h)/i ) {
            NPStatus::Services::setIntVip_OServices( $line );
        }
        elsif ( $line =~ /P/i ) {
            NPStatus::Services::setIntVip_PServices( $line );
        }
        else { print "Only valid lines are H1, H2, P1, P2, e1\n"; exit 255; }
    }
    else { print "Unrecognized test type '" . $test_type . "'\n"; exit 255; }

    # If list flag was passed, print details then exit
    if( $list_all ) {
        listAvailableServices();
        exit 1;
    }

    # Parse output_format flag correctly
    switch( $output_format ) {
        case /(utf8|json|xml)/  { ; }
        else                    { print "Output format '" . $output_format . "' not supported.\n"; exit 255; }
    }

    # Parse user_group flag correctly
    switch( $user_group ) {
        case /(cpo|spo|lat)/    { ; }
        else                    { print "User group '" . $user_group . "' not supported.\n"; exit 255; }
    }
    print "User Group: " . $user_group . "\n";   

    # Does the app specified exist, and hosts exist.
    if( defined( $service_name ) && length( $service_name ) > 0 ) {
        # Does this app exist?
        unless( defined( $NPStatus::Services::services->{ $service_name } ) && length( $NPStatus::Services::services->{ $service_name } ) > 0 ) { 
            print "Service \"$service_name\" is not valid.\n";
            listAvailableServices();
            exit 255;
        }
        # Does this service have any hosts?
        # we don't have a nifty way to abstract the tier to get a host, so manually check tier flag and set variable accordingly.
        my $tiervar = "tier".$tier;
        $tiervar = "tier1" if ( looks_like_number($tier) && $tier == 0 );
        $tiervar = "tier1" if ( ! looks_like_number($tier) && ( $tier eq 'tomcat' || $tier eq 'apache'));
        unless( (defined( $NPStatus::Services::services->{ $service_name }->{hosts}->{$tiervar} ) && scalar( @{ $NPStatus::Services::services->{ $service_name }->{hosts}->{$tiervar} } ) > 0)
        || (defined( $NPStatus::Services::services->{ $service_name }->{hosts}->{tier2} ) && scalar( @{ $NPStatus::Services::services->{ $service_name }->{hosts}->{tier2} } ) > 0)) { 
            print "Service \"$service_name\" for tier " . $tiervar . " has no hosts.\n";
            listAvailableServices();
            exit 255;
        }
    }

    # Initialize the username and password for SSH
    setEnvironmentVars();

    # Finish setting up service and hostnames
    NPStatus::Services::setServiceNames();

    # Perform a login test
    unless( $test_type eq "intvip" || $test_type eq "extvip" ) {

        # we don't have a nifty way to abstract the tier to get a host, so manually check tier flag and set variable accordingly.
        my $tiervar = "tier".$tier;
    	$tiervar = "tier1" if ( looks_like_number($tier) && $tier == 0 );
        $tiervar = "tier1" if ( ! looks_like_number($tier) && ($tier eq 'tomcat' || $tier eq 'apache'));
        
        # Some services don't have tier1 defined yet, so detect
        if( ! defined($NPStatus::Services::services->{ $NPStatus::Services::setservices[0] }->{hosts}->{$tiervar})) {
            $tiervar = "tier2";
        }

        for my $host ( sort @{ $NPStatus::Services::services->{ $NPStatus::Services::setservices[0] }->{hosts}->{$tiervar} } ) {

            my $dnslookup = `dig +short $host`;
            unless( $dnslookup =~ /\d+\.\d+\.\d+\.\d+/ ) { NPStatus::Core::debug("test host login: invalid dns $host\n"); next; }
            else {

                NPStatus::Core::debug( "test host login: dns succeeded\n");
                my $exitcode = NPStatus::Core::testHostLogin( $host, 0, $sshusername, $sshpassword );
                unless( $exitcode == 0 || $exitcode == 101 ) {

                    my $errtext = '';
                    if( $exitcode == 100 ) {
                        $errtext = "AD login not configured";
                    }
                    elsif( $exitcode == 107 ) {
                        $errtext = "Invalid AD password...Exiting\n";
                    }
                    elsif( $exitcode == 108 ) {
                        $errtext = "Account locked";    
                    }
                    elsif( $exitcode == 109 ) {
                        $errtext = "SSH RSA Fingerprint changed on $host";    
                    }
                    elsif( $exitcode == 110 ) {
                        $errtext = "Login timeout";
                    }
                    elsif( $exitcode == 255 ) {
                        $errtext = "Aborted";
                    }
                    else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

                    print "err: " . $errtext . "\n";
                    exit 255;
                }
                else { NPStatus::Core::debug("test host login: success $host\n"); last; }
            }
            
        }
    }

    return 1;
}

sub listAvailableServices {

    print "Available services for " . $line . "-" . $environment .":\n";
    printf "%-20s %-10s %-7s %-12s %-7s %-25s\n", "Name", "Class", "Tiers", "DB Facing", "Group", "Description";
    print "-" x 80 . "\n";

    my %services = %$NPStatus::Services::services;
    foreach my $service ( sort( keys %services )) {

        my $dbfacing = 'no';
    	my @available_tiers;

        
        if( $NPStatus::Services::services->{$service}->{dbfacing} ) { $dbfacing = 'yes';}
    	if( defined($NPStatus::Services::services->{$service}->{tier1}) ) { push(@available_tiers, "1");}
    	if( defined($NPStatus::Services::services->{$service}->{tier2}) ) { push(@available_tiers, "2");}
    	my $available_tiers = join(",", @available_tiers);
        # print "service: " . $service;
        # print "class: " . $services{$service}->{class};
        # print "tiers: " . $available_tiers;
        # print "dbfacing: " . $dbfacing;
        # print "user group: " . $services{$service}->{user_group};
        # print "description: " . $services{$service}->{description};
        printf "%-20s %-10s %-7s %-12s %-7s %-25s\n", $service, $services{$service}->{class}, $available_tiers, $dbfacing, $services{$service}->{user_group}, $services{$service}->{description} if defined($services{$service});
    }
}

sub setEnvironmentVars {

    # get the password, but do not let the world know what it is
    if( ! (defined($ENV{'ADPW'}) || defined($ENV{'NPSTAT_ADPW'})) || ! (defined($ENV{'ADUSER'}) || defined($ENV{'NPSTAT_ADUSER'})) ) { print "Consider setting the environment vars NPSTAT_ADPW & NPSTAT_ADUSER.\n"; }

    if( $ENV{'ADPW'} || $ENV{'NPSTAT_ADPW'} ) {
        $NPStatus::Init::sshpassword = $ENV{'ADPW'} if defined($ENV{'ADPW'});
        $NPStatus::Init::sshpassword = $ENV{'NPSTAT_ADPW'} if defined($ENV{'NPSTAT_ADPW'});
    } else {
        my $password = '';
        print "AD password: ";
        system('stty', '-echo');
        chop($password = <STDIN>);
        system('stty', 'echo');
        print "\n";
        $NPStatus::Init::sshpassword = $password;
    }

    # Get the username
    if( $ENV{'ADUSER'} || $ENV{'NPSTAT_ADUSER'} ) {
        $NPStatus::Init::sshusername = $ENV{'ADUSER'} if defined($ENV{'ADUSER'});
        $NPStatus::Init::sshusername = $ENV{'NPSTAT_ADUSER'} if defined($ENV{'NPSTAT_ADUSER'});
    } else {
        my $username = '';
        print "AD username: ";
        system('stty', '-echo');
        chop($username = <STDIN>);
        system('stty', 'echo');
        print "\n";
        $NPStatus::Init::sshusername = $username;
    }

    # Get the user's group
    if( $ENV{'NPSTAT_GROUP'} ) {
        $NPStatus::Init::user_group = $ENV{'NPSTAT_GROUP'};
    }
}

1;  # don't forget to return a true value from the file
