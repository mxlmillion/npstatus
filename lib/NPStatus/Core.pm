package NPStatus::Core;

use strict;
use warnings;
use Data::Dumper;
use Switch;
use List::Util qw(first max maxstr min minstr reduce shuffle sum);
use NPStatus::Services;

our $year = '';
our $mon = '';
our $v1 = '';
our $day = '';
our $hr = '';
our $min = '';
our $sec = '';
our $Date = '';
our $count = 0;
our $success = 0;
our $failure = 0;
our $Begin = 0;
our $services;

sub debug {

	my $level = 1;
	if( $#_ eq 1 ) {
		$level = shift;
	}
	print "debug" . $level . ": " . $_[0] if $level le $NPStatus::Init::debug;
}

sub testService {

	my ($service_name, $tier, $host, $port) = @_;
	my $password = $NPStatus::Init::sshpassword;
	my $username = $NPStatus::Init::sshusername;
	my $svStart = $Begin;
	my $exitcode;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;

	debug( "testService()\n" );
	debug( "child shm dir path: " . $child_shm_path . "\n" );

	print "$service_name $tier on $host:$port..." unless( $NPStatus::Init::parallelize );

	# If DNS exists, proceed with testing
	return if testHostDNS( $host, $port );

	# Run whatever tests the user specified on the command line
	if( $NPStatus::Init::test_type eq "scrape" ) {

		if( $NPStatus::Services::services->{$service_name}->{$tier} eq "tomcat" ) {

			# is the service running?
			if( $service_name eq "zkeeper") {
				return if isProcRunning( $host, "java", "zoo.cfg", "zookeeper", $username, $password );
			} elsif( $service_name eq "mqp") {
				return if isProcRunning( $host, "java", "activemq.jar", "activemq.jar start", $username, $password );
			} elsif( $service_name eq "gcim_auth") {
				return if isProcRunning( $host, "java", "instance=auth", "Bootstrap start", $username, $password );
			} elsif( $service_name eq "gcim_resource") {
				return if isProcRunning( $host, "java", "instance=resource", "Bootstrap start", $username, $password );
			} elsif( $service_name eq "gcim_batch") {
				return if isProcRunning( $host, "java", "instance=batch", "Bootstrap start", $username, $password );
			} elsif( $service_name eq "guide") {
				return if isTomcatRunning( $host, $NPStatus::Services::services->{$service_name}->{jolokia_port}, $username, $password );
			} else {
				return if isTomcatRunning( $host, $port, $username, $password );
			}
		} elsif( $NPStatus::Services::services->{$service_name}->{$tier} eq "apache" ) {
			if( $service_name eq "consolegw") {
				return if isProcRunning( $host, "httpd.worker", "SNP_APP_NAVIGATOR ", "navigator.conf", $username, $password );
			} elsif ($service_name eq "oauth") {
				return if isProcRunning( $host, "httpd.worker", "SNP_APP_NAVIGATOR-OAUTH ", "navigator-oauth.conf", $username, $password );
			}
		}

		# Get status data
		my @cmdresult = getStatusResult( $tier, $host, $port, $username, $password, $service_name );
		return if $#cmdresult == -1;

		# If we want the raw results, just save and exit
		if( $NPStatus::Init::raw_report ) {

			if( $NPStatus::Init::parallelize ) {
				debug("opening file $child_shm_path\n");
				open( FILE, "> $child_shm_path" ) || debug "$!";
				print FILE @cmdresult;
				close FILE;
			} else { print @cmdresult }

			return;
		}

		# Parse results
		my ( $SUCCESS_ref, $FAILURE_ref, $totalsuccess, $totalfail, $appversion, $error_text, $uptime ) = NPStatus::Report::parseCommandResult( $tier, $host, $port, $service_name, @cmdresult );
		my %SUCCESS = %$SUCCESS_ref;
		my %FAILURE = %$FAILURE_ref;
		debug( "\%SUCCESS: " . %SUCCESS . "\n" );
		debug( "\%FAILURE: " . %FAILURE . "\n" );
		debug( "\$totalsuccess: " . $totalsuccess . "\n" );
		debug( "\$totalfail: " . $totalfail . "\n" );
		debug( "\$appversion: " . $appversion . "\n" );
		debug( "\$uptime: " . $uptime . "\n" );

		# If we're in parallel mode, write results to the filesystem for later retrieval
		if( $NPStatus::Init::parallelize ) {

			use IO::File;

			# After results are gathered, stuff data into file
			debug("opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" ) || debug "$!";

			debug( "adding data to file\n" );

			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "tsuccess|" . $totalsuccess . "\n";
			print FILE "tfail|" . $totalfail . "\n";
			print FILE "service_name|" . $service_name . "\n";
			print FILE "hostname|" . $host . "\n";
			print FILE "appversion|" . $appversion . "\n";
			print FILE "uptime|" . $uptime . "\n";
			print FILE "err|" . $error_text . "\n" if defined($error_text);

			# Finally, loop through
			foreach my $key ( keys %SUCCESS ){
				print FILE "success|" . $key . "|" . $SUCCESS{$key} . "\n";
			}
			foreach my $key ( keys %FAILURE ){
				print FILE "fail|" . $key . "|" . $FAILURE{$key} . "\n";
			}

			close(FILE);
		}
		else {

			# Continue as normal
			if ( $totalsuccess == 0 && $totalfail == 0 ) {
				if( length($error_text) eq 0 ) {
					print "err: JVM not serving requests. 0 successes, 0 failures.\n";
				} else { print "err: " . $error_text . "\n"; }
			} else {

				print "had " . $totalsuccess . " successes and " . $totalfail . " failures. ";
				if( defined($appversion) && length($appversion) > 0 ) {
					print "version: $appversion ";
				}
				if( defined($uptime) && length($uptime) > 0 ) {
					print "uptime: " . $uptime . " ";
				}
				print "\n";
			}
		}
	} elsif( $NPStatus::Init::test_type eq "intvip" || $NPStatus::Init::test_type eq "extvip" ) {

		# Get status data
		my @cmdresult = getVIPStatusResult( $tier, $host, $port, $username, $password, $service_name );
		return if $#cmdresult == -1;

		# If we want the raw results, just save and exit
		if( $NPStatus::Init::raw_report ) {

			if( $NPStatus::Init::parallelize ) {
				debug("opening file $child_shm_path\n");
				open( FILE, "> $child_shm_path" ) || debug "$!";
				print FILE @cmdresult;
				close FILE;
			} else { print @cmdresult }

			return;
		}

		# Parse results
		my ( $SUCCESS_ref, $FAILURE_ref, $totalsuccess, $totalfail, $appversion, $error_text, $uptime ) = NPStatus::Report::parseCommandResult( $tier, $host, $port, $service_name, @cmdresult );
		my %SUCCESS = %$SUCCESS_ref;
		my %FAILURE = %$FAILURE_ref;
		debug( "\%SUCCESS: " . %SUCCESS . "\n" );
		debug( "\%FAILURE: " . %FAILURE . "\n" );
		debug( "\$totalsuccess: " . $totalsuccess . "\n" );
		debug( "\$totalfail: " . $totalfail . "\n" );
		debug( "\$appversion: " . $appversion . "\n" );
		debug( "\$uptime: " . $uptime . "\n" );

		# If we're in parallel mode, write results to the filesystem for later retrieval
		if( $NPStatus::Init::parallelize ) {

			use IO::File;

			# After results are gathered, stuff data into file
			debug("opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" ) || debug "$!";

			debug( "adding data to file\n" );

			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "tsuccess|" . $totalsuccess . "\n";
			print FILE "tfail|" . $totalfail . "\n";
			print FILE "service_name|" . $service_name . "\n";
			print FILE "hostname|" . $host . "\n";
			print FILE "appversion|" . $appversion . "\n";
			print FILE "uptime|" . $uptime . "\n";
			print FILE "err|" . $error_text . "\n" if defined($error_text);

			# Finally, loop through
			foreach my $key ( keys %SUCCESS ){
				print FILE "success|" . $key . "|" . $SUCCESS{$key} . "\n";
			}
			foreach my $key ( keys %FAILURE ){
				print FILE "fail|" . $key . "|" . $FAILURE{$key} . "\n";
			}

			close(FILE);
		}
		else {

			# Continue as normal
			if ( $totalsuccess == 0 && $totalfail == 0 ) {
				print "err: JVM not serving requests. 0 successes, 0 failures.\n";
			} else {

				print "had " . $totalsuccess . " successes and " . $totalfail . " failures. ";
				if( defined($appversion) && length($appversion) > 0 ) {
					print "version: $appversion ";
				}
				if( defined($uptime) && length($uptime) > 0 ) {
					print "uptime: " . $uptime . " ";
				}
				print "\n";
			}
		}
	} elsif( $NPStatus::Init::test_type eq "system" ) {

		# Get status data
		my @cmdresult = getSystemTestResults( $host, $port, $username, $password, $service_name );
		return if $#cmdresult == -1;

		# If we want the raw results, just save and exit
		if( $NPStatus::Init::raw_report ) {

			if( $NPStatus::Init::parallelize ) {
				debug("opening file $child_shm_path\n");
				open( FILE, "> $child_shm_path" ) || debug "$!";
				print FILE @cmdresult;
				close FILE;
			} else { print @cmdresult }

			return;
		}

		# Parse results
		my ( $SUCCESS_ref, $FAILURE_ref, $totalsuccess, $totalfail, $error_text ) = NPStatus::Report::parseCommandResult( $service_name, @cmdresult );
		my %SUCCESS = %$SUCCESS_ref;
		my %FAILURE = %$FAILURE_ref;

		# If we're in parallel mode, write results to the filesystem for later retrieval
		if( $NPStatus::Init::parallelize ) {

			use IO::File;

			# After results are gathered, stuff data into file
			debug("opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" ) || debug "$!";

			debug( "adding data to file\n" );

			print FILE "host|" . $host . "\n";
			print FILE "tsuccess|" . $totalsuccess . "\n";
			print FILE "tfail|" . $totalfail . "\n";
			print FILE "service_name|" . $service_name . "\n";
			print FILE "hostname|" . $host . "\n";
			print FILE "err|" . $error_text . "\n" if defined($error_text);

			# Finally, loop through
			foreach my $key ( keys %SUCCESS ){
				print FILE "success|" . $key . "|" . $SUCCESS{$key} . "\n";
			}
			foreach my $key ( keys %FAILURE ){
				print FILE "fail|" . $key . "|" . $FAILURE{$key} . "\n";
			}

			close(FILE);
		}
		else {

			# Continue as normal
			if ( $totalsuccess == 0 && $totalfail == 0 ) {
				if( length($error_text) eq 0 ) {
					print "err: JVM not serving requests. 0 successes, 0 failures.\n";
				} else { print "err: " . $error_text . "\n"; }
			} else {
				print "had " . $totalsuccess . " successes and " . $totalfail . " failures.\n";
			}
		}
	}

	return 1;

} # end - testService

sub getStatusCommand {

	my ( $tier, $host, $port, $service_name, $username, $password ) = @_;

	if ( $NPStatus::Init::test_type =~ /vip/ ) {
		my $protocol = $port == 80 ? "http" : "https";
		return qq(curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -s --show-error -x "" -k "$protocol://$username:$password\@$host/internal/status/html");
	}
	else {
		my $hostname = $username . "@" . $host;

		if( $NPStatus::Services::services->{$service_name}->{$tier} eq 'tomcat' ) {
			switch( $service_name ) {

				#case /zkeeper/  	{ return qq(echo \\"|zkeeper-client-connection-count|\\"; echo stat | nc localhost 2181; echo \\"|end|\\"; echo \\"|exhibitor-state|\\"; curl --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" \\"http://localhost:8080/exhibitor/v1/cluster/state\\";echo;echo \\"|end|\\") }
				case /mqp/			{ return qq(echo \\"|mqp-state|\\";curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -i -u admin:admin -x \\"\\" 'http://localhost:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,service=PersistenceAdapter,instanceName=*,view=Replication'; echo;echo \\"|end|\\")}
				case /gcim/			{ return getGcimTestCmd($port); }
				case /ias/			{ return qq(curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" \\"http://localhost:$port/internal/status\\"); }
				else 				{ return qq(curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" \\"http://localhost:$port/internal/status/html\\"); }

			}
		} elsif ( $NPStatus::Services::services->{$service_name}->{$tier} eq 'apache' ) {
			switch( $service_name ) {
				case /consolegw/	{ return qq(curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" -H \\"Content-Type: application/x-www-form-urlencoded\\" \\"http://localhost:$port/getStatus\\"); }
				else				{ return qq(curl --user-agent $NPStatus::Init::curl_agent_name --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" \\"http://localhost:$port/jkstatus?mime=xml\\"); }
			}
		}
	}
}

sub getSystemTestCommand {

	my ( $host, $port, $service_name, $username, $password ) = @_;

    return qq(echo \\"|splunkdcheck|\\"; ps -C splunkd -o command; echo \\"|end|\\"; echo \\"|cyberarkcheck|\\"; ps -C appprovider -o command; echo \\"|end|\\"; echo \\"|introscope|\\"; ps -ef \| grep introscope \| grep -v grep; echo \\"|end|\\");

}

sub getGcimTestCmd {

	my ($port) = @_;
	my $b64cmd = qq(STATUSIP=\\\$(netstat -lnt | grep $port | awk '{print \\\$4}' | awk -F: '{print \\\$1}') && STATUSPORT=\\\$(netstat -lnt | grep $port | awk '{print \\\$4}' | awk -F: '{print \\\$2}') && curl --max-time $NPStatus::Init::global_timeout -s --show-error -x \\"\\" http://\\\$STATUSIP:\\\$STATUSPORT/01.00/internal/status | sed "s/,/\\n/g");
	$b64cmd = `echo -n \"$b64cmd\" | base64`;
	$b64cmd =~ s/\n/\\n/g;
	$b64cmd =~ s/\\n//g;
	return qq(echo -n \\"$b64cmd\\" | base64 -d | bash -s);
}

sub evalExitCode {

	my $exitcode = shift;
	my $high_byte = $exitcode >> 8;
	debug( "evalExitCode(): $high_byte\n" );
	return $high_byte;
}

sub setMaxForkedProcs {

	# To get around ssh limitations in our system, we have to adjust $max_forked_processes because we can only hold open
	# 5 ssh connections to one host at most, at a time

	my $service_name = shift;

	if( $service_name eq "navpc" || $service_name eq "mailtmpl" ) {
		return 5;
	}
	elsif( $NPStatus::Init::line eq "l1" ) {
		return 5;
	}
	elsif( $NPStatus::Init::host_range ) {
		return 5;
	} else {
		return $NPStatus::Init::max_forked_processes;
	}

}

sub testSSHLogin {

	my ( $host, $port, $username, $password ) = @_;
	debug( "testSSHLogin(): params: $host $port $username $password\n" );
	my $exitcode = 0;
	$port = "sony-sites" if $port eq 0;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostloginscript = qq(
expect -c 'trap {exit 255} SIGINT
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked" { exit 108 }
        "Active Directory Password: \$"   { send "$password\r"; }
        "Password: \$"                    { exit 100; }
        "]\$ "								{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Account locked" { exit 108 }
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { send "ps -C java -o command | grep $port\r" }
        timeout { exit 110 }
};
expect {
        "Bootstrap start"       { exit 0 }
        "]\$ "                   { exit 101 }
        timeout                 { exit 110}
};' > /dev/null
);

	system($hostloginscript);
	$exitcode = evalExitCode($?);
	debug( "$host:$port host login exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Login timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return $exitcode;
	}

	return $exitcode;
}

sub testHostLogin {

	my ( $host, $port, $username, $password ) = @_;
	debug( "testHostLogin(): params: $host $port $username $password\n" );
	my $exitcode = 0;
	$port = "sony-sites" if $port eq 0;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostloginscript = qq(
expect -c 'trap {exit 255} SIGINT
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked" { exit 108 }
        "Active Directory Password: \$"   { send "$password\r"; }
        "Password: \$"                    { exit 100; }
        "]\$ "								{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Account locked" { exit 108 }
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { exit }
        timeout { exit 110 }
};' > /dev/null
);

	system($hostloginscript);
	$exitcode = evalExitCode($?);
	debug( "$host:$port host login exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Login timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return $exitcode;
	}

	return $exitcode;
}

sub isTomcatRunning {

	my ( $host, $port, $username, $password ) = @_;
	debug( "isTomcatRunning(): params: $host $port $username $password\n" );
	my $exitcode = 0;
	$port = "sony-sites" if $port eq 0;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostloginscript = qq(
expect -c 'trap {exit 255} SIGINT
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked" { exit 108 }
        "Active Directory Password: \$"   { send "$password\r"; }
        "Password: \$"                    { exit 100; }
        "]\$ "								{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Account locked" { exit 108 }
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { send "ps -C java -o command | grep $port\r" }
        timeout { exit 110 }
};
expect {
		"/usr/bin/java"         { exit 0 }
        "Bootstrap start"       { exit 0 }
        "]\$ "                  { exit 101 }
        timeout                 { exit 110}
};' > /dev/null
);

	system($hostloginscript);
	$exitcode = evalExitCode($?);
	debug( "$host:$port host login exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Login timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return $exitcode;
	}

	return $exitcode;
}

sub isProcRunning {

	my ( $host, $proc, $grep, $expgrep, $username, $password ) = @_;
	debug( "isProcRunning(): params: $host $proc $grep $username $password\n" );
	my $exitcode = 0;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostloginscript = qq(
expect -c 'trap {exit 255} SIGINT
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked" { exit 108 }
        "Active Directory Password: \$"   { send "$password\r"; }
        "Password: \$"                    { exit 100; }
        "]\$ "								{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Account locked" { exit 108 }
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { send "ps -C $proc -o command | grep $grep | grep -v grep\r" }
        timeout { exit 110 }
};
expect {
        "$expgrep"       { exit 0 }
        "]\$ "                   { exit 101 }
        timeout                 { exit 110}
};' > /dev/null
);

	system($hostloginscript);
	$exitcode = evalExitCode($?);
	debug( "$host host login exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Login timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return $exitcode;
	}

	return $exitcode;
}

sub testHostDNS {

	debug( "testHostDNS()\n" );

	my $host = shift;
	my $port = shift;
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $dnslookup = `dig +short $host`;
	unless( $dnslookup =~ /\d+\.\d+\.\d+\.\d+/ ) {

		if( $NPStatus::Init::parallelize ) {

			debug("encounted dns err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" ) || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|No such host. Failed DNS lookup.\n";
			close FILE;
		}
		else {
			print "No such host. Failed DNS lookup\n";
			$NPStatus::Report::badcount++;
			push( @NPStatus::Report::BADHOST, $host );
		}

		return 1;
	}
	debug( "nslookup resolved DNS for host.\n" );

	return 0;
}

sub getStatusResult {

	debug( "getStatusResult()\n" );

	my $tier = shift;
	my $host = shift;
	my $port = shift;
	my $username = shift;
	my $password = shift;
	my $service_name = shift;
	my $cmd = getStatusCommand( $tier, $host, $port, $service_name, $username, $password);
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostscript = qq(
expect -c 'trap {exit 255} SIGINT;
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked"				{ exit 108 }
        "Active Directory Password: \$" { send "$password\r"; }
        "Password: \$"                  { exit 100; }
        "]\$ "							{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { send "$cmd\\r" }
        timeout { exit 110 }
};
while 1 {
	expect {
		"curl: (7)"   { exit 111 }
	    default {
	        append output \$expect_out(buffer)
	    }
	    "]\$ " {
	        append output \$expect_out(buffer)
	        break
	    }
	}
};');

	debug( 2, "cmd: $hostscript\n" );

	my @cmdresult = `$hostscript`;
	debug( 3, "result:\n");
	debug( 3, join(" ", @cmdresult));
	my $exitcode = evalExitCode($?);
	debug( "$host:$port cmd exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 111 ) {
			$errtext = "Service port not open: Couldn't connect";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return;
	}

	return @cmdresult;
}

sub getVIPStatusResult {

	debug( "getVIPStatusResult()\n" );

	my $tier = shift;
	my $host = shift;
	my $port = shift;
	my $username = shift;
	my $password = shift;
	my $service_name = shift;
	my $cmd = getStatusCommand( $tier, $host, $port, $service_name, $username, $password);
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;

	debug( 2, "cmd: $cmd\n" );

	my @cmdresult = `$cmd`;
	my $exitcode = evalExitCode($?);
	debug( "$host:$port cmd exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return;
	}

	return @cmdresult;
}

sub getSystemTestResults {

	debug( "getSystemTestResults()\n" );

	my $host = shift;
	my $port = shift;
	my $username = shift;
	my $password = shift;
	my $service_name = shift;
	my $cmd = getSystemTestCommand( $host, $port, $service_name, $username, $password);
	my $child_shm_path = $NPStatus::Init::rand_childshm_dir . "/npstatus." . $$;
	my $hostscript = qq(
expect -c 'trap {exit 255} SIGINT;
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no $username\@$host;
expect {
		"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED" { exit 109 }
		"Account locked"				{ exit 108 }
        "Active Directory Password: \$" { send "$password\r"; }
        "Password: \$"                  { exit 100; }
        "]\$ "							{ send "\r"; }
        timeout                         { exit 110; }
};
expect {
		"Active Directory Password: \$"   { exit 107 }
        "]\$ "   { send "$cmd\\r" }
        timeout { exit 110 }
};
while 1 {
	expect {
	    default {
	        append output \$expect_out(buffer)
	    }
	    "]\$ " {
	        append output \$expect_out(buffer)
	        break
	    }
	}
};');

	debug( 2, "cmd: $cmd\n" );

	my @cmdresult = `$hostscript`;
	my $exitcode = evalExitCode($?);
	debug( "$host:$port cmd exit code: " . $exitcode . "\n" );
	if( $exitcode != 0 ) {

		my $errtext = '';
		if( $exitcode == 100 ) {
			$errtext = "AD login not configured";
		}
		elsif( $exitcode == 107 ) {
			print "Invalid AD password...Exiting\n";
			exit 107;
		}
		elsif( $exitcode == 108 ) {
			$errtext = "Account locked";
		}
		elsif( $exitcode == 109 ) {
			$errtext = "SSH RSA Fingerprint changed.";
		}
		elsif( $exitcode == 110 ) {
			$errtext = "Timeout";
		}
		elsif( $exitcode == 101 ) {
			$errtext = "Service not running";
		}
		elsif( $exitcode == 255 ) {
			#print "Aborted";
			exit 255;
		}
		else { $errtext = "Unhandled Condition (unknown): " . $exitcode; }

		if( $NPStatus::Init::parallelize ) {

			debug("encounted err, opening file $child_shm_path\n");
			open( FILE, "> $child_shm_path" )  || debug "$!";
			debug( "adding data to file\n" );
			print FILE "host|" . $host . "\n";
			print FILE "port|" . $port . "\n";
			print FILE "err|" . $errtext . "\n";
			close FILE;
		}
		else {
			print "err:" . $errtext . "\n";
		}

		return;
	}

	return @cmdresult;
}

sub usage {

	if ( $#ARGV >= 2 ) {
		print
"\nArguments Found\nSide:$ARGV[0]\nEnvironment:$ARGV[1]\nService:$ARGV[2]\n[the following Host range parameters are optional\nbut must include BOTH if used]\nStartHost:$NPStatus::Init::host_range_start\nEndHost:$NPStatus::Init::host_range_end\n\n";
	}

	print <<"USAGE";

USAGE\n
	$0 <flags>
	$0 -h/--help\n
FLAGS\n

	-D/--debug
		Prints out debug info. Use multiple times to increase verbosity.

	-e/--environment <string>
		Specifies the target enivronment. Valid values are: 'np' & 'pl'. 'np' is default.

	--format/--output-format <string>
		Specifies how results are printed. Valid values are: 'utf8' & 'json'. 'utf8' is default.

		JSON support is experimental

	--group/-g
		Changes user's group. Running bulk commands like --all, --unsafe, etc. run only apps that are part of your group.

		Available groups are: spo, cpo, lat, hawkeye

		'spo' is the default group.

	-h/--help
		Prints this message. This message is also printed when no arguments are supplied.

	-l/--line <string>
		The line you want to test against. For example: p1, p2, e1

	-L/--list
		Print out a list of avaible services for a line.

	--poll
		Continuously run the test. Use ctrl-c to abort.

	-P/--parallel
		Specifies whether status should utilize fork() to speed up testing of a service that has a large number of instances.

	-r/--range <start int>-[<end int>]

		To scan only host 1: -r 1-1
		To scan hosts 23-43: -r 23-43
		To scan all hosts except the first 8: -r 9-

	--raw
		Do not parse returned content, but instead return raw output from status endpoint.

	-s/--service-name <string>
		The application (or service) to test. To get a list of available services, use the -l flag.

	-t/--test-type <string>
		Test type. By default, this is the "scrape" functionality, where each instance's /internal/status/html endpoint is queried
		and evaluated. There are 3 test types:

		scrape - Evaluate each instance's /internal/status/html endpoint.
		extvip (not ready) - Like scrape, but evaluates the external VIP address.
		intvip (not ready) - Like scrape, but evaluates a VIP that is only accessible within our network.

	--test-all/--all
		Tests all available services the script knows about.


	--test-all-safe/--safe
		Tests only apps that are Safe.


	--test-all-unsafe/--unsafe
		Tests only apps that are Unsafe.

	--tier <string>
		Specify which tier of the service to test.

		Available tiers: tomcat, apache

		Alternatively, a numerical value can be passed as well. For example, to test tier 1 of the Oauth application, which is the
		Apache servers, pass either numeric '1' or the string 'apache'.


ENVIRONMENT VARIABLES\n\n
	\$ADPW\n\t\$NPSTAT_ADPW\t\tUsed for SSH logins and Basic auth for VIP checks.\n
	\$ADUSER\n\t\$NPSTAT_ADUSER\t\tUsed for SSH logins and Basic auth for VIP checks.\n
	\$NPSTAT_GROUP\t\tConfigures your group membership. Running bulk commands like --all, --unsafe, etc. \n\t\t\t\trun only apps that
	are part of your group. 'spo' is the default group.\n
USAGE
	exit 1;
}

sub cleanup {
	opendir( SHM_DIR, $NPStatus::Init::rand_childshm );
	my @dirs = `find $NPStatus::Init::rand_childshm -maxdepth 1 -user $ENV{USER}`;
	foreach my $dir (@dirs) {

		next if $dir eq "." || $dir eq "..";
		chomp($dir);
		my @stats = stat($dir);
		my $tnow = time();
		if ( $stats[9] <= ($tnow - $NPStatus::Init::child_shm_ttl) ) {
			NPStatus::Core::debug( "deleting: " . $dir . "\n" );
			system("rm -rf " . $dir)
		}
	}

	closedir SHM_DIR;
}

1;
