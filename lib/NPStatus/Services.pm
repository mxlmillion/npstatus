package NPStatus::Services;

use List::Util qw(first max maxstr min minstr reduce shuffle sum);

our @setservices;
our $services;


sub setServiceProperties {

	$services->{msg_gateway_xmb} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Korra - Message Gateway XMB Notification Processor",
		tier1 => 'tomcat'
	};
	$services->{token} = {
		class => "safe",
		dbfacing => 1,
		user_group => "cpo",
		description => "Credit Card Tokenizer Service",
		tier1 => 'tomcat'
	};
	$services->{guide} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "SPGS",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_guide:461'],
		jolokia_port => '14525'
	};
	$services->{console_notification} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Notification Service for Console Gateway",
		tier1 => 'tomcat'
	};
	$services->{wallets} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Korra - Wallets Service",
		tier1 => 'tomcat'
	};
	$services->{consolegw} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Console Gateway (KDS)",
		tier1 => 'apache'
	};
	$services->{gcim_batch} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "GCIM Batch Job Processor",
		tier2 => 'tomcat'
	};
	$services->{gcim_resource} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "GCIM Resource/Accounts Service",
		tier2 => 'tomcat'
	};
	$services->{gcim_auth} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "GCIM Authentication Service",
		tier2 => 'tomcat'
	};
	$services->{mqp} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Message Queue Platform (activemq)",
		tier1 => 'tomcat'
	};
	# $services->{zkeeper} = {
	# 	class => "safe",
	# 	dbfacing => 0,
	# 	user_group => "spo",
	# 	description => "",
	# 	tier1 => 'tomcat'
	# };
	$services->{navtax} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "cpo",
		description => "Korra - Tax Service",
		tier1 => 'tomcat'
	};
	$services->{queue_adaptor} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "PS3 Notifications Queue service",
		tier1 => 'tomcat'
	};
	$services->{accounts} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Korra - Accounts Service",
		tier1 => 'tomcat'
	};
	$services->{ingester} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "lat",
		description => "Media Ingester",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_ingester:11443']
	};
	$services->{solr} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "cpo",
		description => "Search Index",
		tier1 => 'tomcat'
	};
	$services->{act} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Account & Commerce Tool, AutoBot",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_act:468']
	};
	# Shivas :: Commenting BIVL service as its decommissioned
	#$services->{bivl} = {
	#	class => "safe",
	#	dbfacing => 0,
	#	user_group => "spo",
	#	description => "Bravia Internet Video Link",
	#	tier1 => 'apache',
	#	tier2 => 'tomcat',
	#	modjk_group => ['ajp_bivl:457']
	#};
	$services->{mailfdbk} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Retries emails that failed to send",
		tier1 => 'tomcat'
	};
	$services->{grc} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Global Reg-Cam account management",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_globalregcam:458']
	};
	$services->{mailmq} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Email Message Queue",
		tier1 => 'tomcat'
	};
	$services->{mailtmpl} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Email Processor",
		tier1 => 'tomcat'
	};
	$services->{link} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Navigator Account Linking/Merging Service",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_link:944']
	};
	$services->{gateway} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Notification Gateway",
		tier1 => 'tomcat'
	};
	$services->{processor} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "spo",
		description => "Notification Processor",
		tier1 => 'tomcat'
	};
	$services->{mds} = {
		class => "safe",
		dbfacing => 1,
		user_group => "hawkeye",
		description => "Merlin DRM Server",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_mds:9055']
	};
	$services->{native} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Store/Auth/User Info service",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_native:455']
	};
	$services->{navpc} = {
		class => "safe",
		dbfacing => 0,
		user_group => "cpo",
		description => "In Game store service",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_navigator:455']
	};
	$services->{oauth} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Authentication for PSN",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_oauth:465']
	};
	$services->{product} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "lat",
		description => "PMT",
		tier2 => 'tomcat'
	};
	$services->{cst2g} = {
		class => "safe",
		dbfacing => 0,
		user_group => "lat",
		description => "Customer Support Tool 2nd gen",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_cst:460']
	};
	$services->{resource} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "PSN Account Resource provider",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_resource:466','ajp_api:462']
	};
	$services->{voucher} = {
		class => "unsafe",
		dbfacing => 1,
		user_group => "cpo",
		description => "Voucher Redemption",
		tier1 => 'tomcat'
	};
	$services->{msg_gateway} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Korra - Message Gateway Frontend",
		tier1 => 'tomcat'
	};
	$services->{msg_gateway_sms} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Korra - Message Gateway SMS Processor",
		tier1 => 'tomcat'
	};
    $services->{msg_gateway_email} = {
        class => "safe",
        dbfacing => 0,
        user_group => "spo",
        description => "Korra - Message Gateway Email Processor",
        tier1 => 'tomcat'
    };
	$services->{cent} = {
		class => "safe",
		dbfacing => 1,
		user_group => "spo",
		description => "Korra - Core Entitlements Service",
		tier1 => 'tomcat'
	};
	$services->{admin} = {
		class => "safe",
		dbfacing => 0,
		user_group => "cpo",
		description => "CST",
		tier1 => 'apache',
		tier2 => 'tomcat',
		modjk_group => ['ajp_admin:460']
	};
	#$services->{reng}->{class} = "safe";
	$services->{authgw} = {
		class => "safe",
		tier1 => 'apache',
		user_group => 'spo',
		description => 'Auth GW'
	};
	$services->{ias} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Korra - Identity Aggregation Service",
		tier1 => 'tomcat'
	};
  $services->{dms} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Korra - Device Management Service",
		tier1 => 'tomcat'
	};
  $services->{cas} = {
		class => "safe",
		dbfacing => 0,
		user_group => "spo",
		description => "Korra - Central Authentication Service",
		tier1 => 'tomcat'
	};
}

# substitute host values for a given pattern and range
# getHosts( "lvn-S-npnvlnktH", $_[0], 1, 2 )
sub getHosts {

	my @hosts;
	my $start = $NPStatus::Init::host_range_start > 0 ? $NPStatus::Init::host_range_start : $_[2];
	my $end = $NPStatus::Init::host_range_end > 0 ? $NPStatus::Init::host_range_end : $_[3];
	my $hname = `hostname`;

	for ( my $i = $start ; $i <= $end ; $i++ ) {
		my $host = $_[0];

		# Replace S with line (e1, p1, p2, etc.)
		$host =~ s/S/$_[1]/;

		# Replace lvp-p1 with new sectorX syntax, lvb-p1, only if on new lvb bastion
		$host =~ s/lvp-p1/lvb-p1/i if $hname =~ m/^lvb/;
		$host =~ s/lvp-uid/lvb-uid/i if $hname =~ m/^lvb/;

		# Retain leading zero '0' if under 10
		if ( $i < 10 ) {
			$host =~ s/H/0H/;
		}
		$host =~ s/H/$i/;
		$host = $host . ".sonynei.net" if ( $host !~ m/\.(net|com|org)$/ );

		push( @hosts, $host );
	}

	return @hosts;
}

# substitute port values for a given pattern and range
sub getPorts {
	my @ports = ();
	for ( my $i = $_[1 ] ; $i <= $_[2] ; $i++ ) {
		my $port = $_[0];
		$port =~ s/P/$i/;
		push( @ports, $port );
	}
	return @ports;
}

sub tomcatTierExists {
	my $sname = shift;
	if( (defined($NPStatus::Services::services->{$sname}->{tier1}) && $NPStatus::Services::services->{$sname}->{tier1} eq 'tomcat') ||
		(defined($NPStatus::Services::services->{$sname}->{tier2}) && $NPStatus::Services::services->{$sname}->{tier2} eq 'tomcat')) {
		NPStatus::Core::debug( "tomcatTierExists $sname: true\n" );
		return 1;
	}
	NPStatus::Core::debug( "tomcatTierExists $sname: false\n" );
	return 0;
}

sub apacheTierExists {
	my $sname = shift;
	if( (defined($NPStatus::Services::services->{$sname}->{tier1}) && $NPStatus::Services::services->{$sname}->{tier1} eq 'apache') ||
		(defined($NPStatus::Services::services->{$sname}->{tier2}) && $NPStatus::Services::services->{$sname}->{tier2} eq 'apache')) {
		NPStatus::Core::debug( "apacheTierExists $sname: true\n" );
		return 1;
	}
	NPStatus::Core::debug( "apacheTierExists $sname: false\n" );
	return 0;
}

sub setServiceNames {
	# Build initial array of services names based on command line flags
	for my $sname (keys %{$NPStatus::Services::services}) {

		NPStatus::Core::debug( "setServiceName: eval $sname\n" );
		my $validname;
		# If only one service requested, add and break
		if ( length($NPStatus::Init::service_name) > 0 && defined($services->{$NPStatus::Init::service_name}) ) {
			push(@setservices, $NPStatus::Init::service_name);
			NPStatus::Core::debug( "pushed $NPStatus::Init::service_name\n" );
			last;
		}

		# Skip if this service is not part of this user's group
		next if $NPStatus::Services::services->{ $sname }->{user_group} ne $NPStatus::Init::user_group;

		# If the --tier flag is set, skip any service that doesn't have the requested tier
		next if $NPStatus::Init::tier eq '1' && ! defined($NPStatus::Services::services->{$sname}->{tier1});
		next if $NPStatus::Init::tier eq '2' && ! defined($NPStatus::Services::services->{$sname}->{tier2});
		next if $NPStatus::Init::tier eq 'tomcat' && ! tomcatTierExists($sname);
		next if $NPStatus::Init::tier eq 'apache' && ! apacheTierExists($sname);

		if( $NPStatus::Init::test_all_services ) { $validname = $sname; }
		elsif( $NPStatus::Init::test_all_safe && $NPStatus::Services::services->{$sname}->{class} eq "safe" ) { $validname = $sname; }
		elsif( $NPStatus::Init::test_all_unsafe && $NPStatus::Services::services->{$sname}->{class} eq "unsafe" ) { $validname = $sname;}
		elsif( $NPStatus::Init::test_dbfacing && $NPStatus::Services::services->{$sname}->{dbfacing} == 1 ) { $validname = $sname; }
		elsif( $NPStatus::Init::test_non_dbfacing && $NPStatus::Services::services->{$sname}->{dbfacing} == 0 ) { $validname = $sname; }

		# perform last checks to make sure we can add this service
		if ( defined( $NPStatus::Services::services->{$validname}->{hosts}) ) {
			push(@setservices, $sname);
			NPStatus::Core::debug( "pushed $sname\n" );
		}
	}

}

sub getShuffledServiceStruct {

	# This constructs the main structure used to feed to the Concurrency module.
	# Each item is work that can be executed in parallel. Each item is grouped under the name of the service it relates to.
	# Each item group is randomized to get around a bug in which the maximum amount of SSH connections is reached on a host with many instances.
	my $service_struct;

	foreach my $sname (@setservices) {

		my @tier1_service_arr;
		my @tier2_service_arr;
		my @shuffled_arr;

		NPStatus::Core::debug( "building host struct for service: $sname\n");
		if( $NPStatus::Init::tier eq 0 ) {
			NPStatus::Core::debug( "all tiers\n");
			for my $host ( @{ $services->{ $sname }->{hosts}->{tier1} } ) {
				for my $port ( @{ $services->{ $sname }->{ports}->{tier1} } ) {
					NPStatus::Core::debug(2, "tier1:$host:$port\n");
					push( @tier1_service_arr, "tier1:" . $host . ":" . $port );
				}
			}
			if ( defined($services->{$sname}->{hosts}->{tier2}) ) {
				for my $host ( @{ $services->{ $sname }->{hosts}->{tier2} } ) {
					for my $port ( @{ $services->{ $sname }->{ports}->{tier2} } ) {
						NPStatus::Core::debug(2, "tier2:$host:$port\n");
						push( @tier2_service_arr, "tier2:" . $host . ":" . $port );
					}
				}
			}
		} elsif( $NPStatus::Init::tier eq 1 ) {
			NPStatus::Core::debug( "tier1\n");
			for my $host ( @{ $services->{ $sname }->{hosts}->{tier1} } ) {
				for my $port ( @{ $services->{ $sname }->{ports}->{tier1} } ) {
					NPStatus::Core::debug(2, "tier1:$host:$port\n");
					push( @tier1_service_arr, "tier1:" . $host . ":" . $port );
				}
			}
		} elsif( $NPStatus::Init::tier eq 2 ) {
			NPStatus::Core::debug( "tier2\n");
			for my $host ( @{ $services->{ $sname }->{hosts}->{tier2} } ) {
				for my $port ( @{ $services->{ $sname }->{ports}->{tier2} } ) {
					NPStatus::Core::debug(2, "tier2:$host:$port\n");
					push( @tier2_service_arr, "tier2:" . $host . ":" . $port );
				}
			}
		} elsif( $NPStatus::Init::tier == 'tomcat' ) {
			my $localtier;
			if( defined($NPStatus::Services::services->{$sname}->{tier1}) && $NPStatus::Services::services->{$sname}->{tier1} eq $NPStatus::Init::tier ){
				$localtier = 'tier1';
			} elsif( defined($NPStatus::Services::services->{$sname}->{tier2}) && $NPStatus::Services::services->{$sname}->{tier2} eq $NPStatus::Init::tier ) {
				$localtier = 'tier2';
			}
			NPStatus::Core::debug( "tomcat, $localtier\n");
			for my $host ( @{ $services->{ $sname }->{hosts}->{$localtier} } ) {
				for my $port ( @{ $services->{ $sname }->{ports}->{$localtier} } ) {
					NPStatus::Core::debug(2, $localtier . ":$host:$port\n");
					push( @tier1_service_arr, $localtier . ":" . $host . ":" . $port );
				}
			}
		} elsif( $NPStatus::Init::tier == 'apache' ) {
			NPStatus::Core::debug( "apache\n");
			my $localtier;
			if( defined($NPStatus::Services::services->{$sname}->{tier1}) && $NPStatus::Services::services->{$sname}->{tier1} eq $NPStatus::Init::tier ){
				$localtier = 'tier1';
			} elsif( defined($NPStatus::Services::services->{$sname}->{tier2}) && $NPStatus::Services::services->{$sname}->{tier2} eq $NPStatus::Init::tier ) {
				$localtier = 'tier2';
			}
			for my $host ( @{ $services->{ $sname }->{hosts}->{$localtier} } ) {
				for my $port ( @{ $services->{ $sname }->{ports}->{$localtier} } ) {
					NPStatus::Core::debug(2, $localtier . ":$host:$port\n");
					push( @tier1_service_arr, $localtier . ":" . $host . ":" . $port );
				}
			}
		}

		# Add the new array which contains all host and port combos to the master struct.
		@shuffled_arr = shuffle( @tier1_service_arr, @tier2_service_arr );
		$service_struct->{$sname} = \@shuffled_arr;
	}

	return $service_struct;
}

sub setExtVip_PServices {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setExtVip_OServices {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier1} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier1}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setIntVip_PServices {

	my $services = setServiceClassification();;

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier1} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier1}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setIntVip_OServices {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier1} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier1}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setO_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier1} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier1}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setO_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setC_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setC_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setC_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setC_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setL_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setL_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setL_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setL_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setH_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setH_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP1_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP1_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP2_NPTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP2_NPTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP1_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP1_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP2_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setP2_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setH_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setH_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...

}

sub setO_PLTier1Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

sub setO_PLTier2Services {

	# Removed Sony specific hostnames, usually this list is much longer
	#
	# populate hostname array
	$services->{example_service}->{hosts}->{tier2} = [ getHosts( "example-S-hostname_H", $_[0], 1, 2 ) ];
	# ...
	#
	# # populate port array
	$services->{example_service}->{ports}->{tier2}	= [ getPorts( "11P80", 0, 0 ) ];
	# ...
}

1;  # don't forget to return a true value from the file
