#!/usr/bin/perl

# Core includes
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname( abs_path $0 ) . "/lib";

use strict;
use warnings;
use Data::Dumper;
use Sys::Hostname;
use Switch;

# Custom libs
use NPStatus::Init;
use NPStatus::Core;
use NPStatus::Report;
use NPStatus::Services;
use NPStatus::Concurrency;

# Init & parse command line options
NPStatus::Init::argInit();


my $service_name = $NPStatus::Init::service_name;
my $service_struct_ref = NPStatus::Services::getShuffledServiceStruct( $service_name );

do {
	for my $service_name (keys %{ $service_struct_ref }) {
		
		# If service has no hosts, print and continue;
		if( scalar(@{$service_struct_ref->{$service_name}}) == 0 ) {
			print "Service " . $service_name . " has no hosts.\n";
			next;
		}

		# Do we want to do parallel execution for speed?
		if( $NPStatus::Init::parallelize ) {

			# Begin
			($NPStatus::Init::report_summary && printf "%-32s", "Testing $service_name...") || ($NPStatus::Init::output_format ne "json" && print "Testing $service_name...\n");
			my $kidpids = NPStatus::Concurrency::execTest( $service_name, $service_struct_ref->{$service_name} );

			NPStatus::Core::debug( "all children executed. collecting data\n" );
			NPStatus::Report::parallelAggregate( $kidpids );

			NPStatus::Core::debug( "calling printStatistics()" );
			NPStatus::Report::printStatistics();
		}
		# No parallel execution, one host at a time
		else {

			foreach ( @{ $service_struct_ref->{ $service_name } } ) {

				my ($tier, $host, $port) = split(":", $_);
				NPStatus::Core::debug( "$service_name on $host\n" );
				NPStatus::Core::debug( "Testing service $service_name on " . $NPStatus::Init::line . "-" . $NPStatus::Init::environment . " on " . $host . ":" . $port . "\n" );
				NPStatus::Core::testService( $service_name, $tier, $host, $port );
			}

			NPStatus::Report::printStatistics();
		}
	}
} while ( $NPStatus::Init::poll );


# Cleanup
NPStatus::Core::cleanup();
